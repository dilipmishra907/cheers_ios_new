//
//  AppDelegate.swift
//  Cheers
//
//  Created by Dilip Mishra on 24/04/20.
//  Copyright © 2020 Dilip Mishra. All rights reserved.
//

import UIKit
import CoreData
import FacebookCore
import FacebookLogin
import FBSDKLoginKit
import GoogleMaps
import CoreLocation



@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,CLLocationManagerDelegate {
    
    let googleApiKey = "AIzaSyCOv7RWmI0enR_46XTnGqVsDMyb3kJGjQI"
    let locationManager = CLLocationManager()


    
    var window: UIWindow?
func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        GMSServices.provideAPIKey(googleApiKey)
    
    self.locationManager.requestAlwaysAuthorization()
    self.locationManager.requestWhenInUseAuthorization()

    if CLLocationManager.locationServicesEnabled() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
    }

        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
    
    if let loginData =  UserDefaults.standard.data(forKey: "LoginDetails") {
               let dict = NSKeyedUnarchiver.unarchiveObject(with: loginData) as! NSDictionary
              
        userDetails = dict as! [String : Any]
         userID = userDetails["id"] as! String
        
        print("userID",userID)

        


           } else {
           }

        
        
     //   FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)

        

       // FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
    
//        let controller = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "HomeNVC") as! HomeNVC
//              controller.navigationController?.navigationBar.isHidden = true
//              let appDelegate = UIApplication.shared.delegate as! AppDelegate
//              appDelegate.window?.rootViewController = controller
//              self.window?.makeKeyAndVisible()

    
    
//    let controller = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "HomeNVC") as! HomeNVC
//                     let appDelegate = UIApplication.shared.delegate as! AppDelegate
//
//    print("controller",controller)
//                     appDelegate.window!.rootViewController = controller
    
    
        
//        if (UserDefaults.standard.bool(forKey: "isLogin"))
//               {
//
//                   let controller = wallStoryBoard.instantiateViewController(withIdentifier: "LandingNVC") as! LandingNVC
//                   let appDelegate = UIApplication.shared.delegate as! AppDelegate
//                   appDelegate.window!.rootViewController = controller
//               }
//               else
//               {
//                   let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//                   let controller = storyBoard.instantiateViewController(withIdentifier: "LandingNVC") as! LandingNVC
//                   let appDelegate = UIApplication.shared.delegate as! AppDelegate
//                   appDelegate.window.rootViewController = controller
//               }

        
        return true
    }
    
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        AppEvents.activateApp()
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        let handled = FBSDKCoreKit.ApplicationDelegate.shared.application(app, open: url, options: options)
        return handled
    }

    

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "Cheers")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
           if let location = locations.last{
               let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            
            
    
            
                       
                       user_latitude =  location.coordinate.latitude
                      user_longitude =  location.coordinate.longitude
        }
    
}

}

