//
//  LeftViewController.swift
//  SlideMenuControllerSwift
//  Created by Yuji Hato on 12/3/14.
//

import UIKit
import Alamofire
import SwiftyJSON


enum LeftMenu: Int
{
    case home = 0
    case myCart
    case favrate
    case myorders
    case settings
    case logOut
}

protocol LeftMenuProtocol : class {
    func changeViewController(_ menu: LeftMenu)
}

class LeftViewController : UIViewController, LeftMenuProtocol
{
    var selectedMenuIndex =  0
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var view_header: UIView!
    var menus = ["Home", "My Cart","Favorite","My Orders","Settings","Sign Out"]
    var imagesArray = ["home", "cart","favrate","myOrder","settings","logout"]
    
    @IBOutlet weak var lbl_email: UILabel!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var lblUserName: UILabel!
    let origin = "LeftViewController"
    var mainViewController: UIViewController!
    var homeViewController: UIViewController!
    var yourProfileViewController: UIViewController!
    var orderDeatilsController : UIViewController!
    var imageHeaderView: ImageHeaderView!
    var categoryController : UIViewController!
    var cartViewController : UIViewController!
    var settingsController : UIViewController!
    var favController : UIViewController!
    
    @IBOutlet weak var typesOfUserView: UIView!
    @IBOutlet weak var lblAppVersion: UILabel!
    var controlerName : String!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        lbl_email?.textColor = UIColor(red: 99/255, green: 99/255, blue: 99/255, alpha: 1.0)
                   lbl_email?.font = UIFont.boldSystemFont(ofSize: 18)
        lblUserName?.textColor = UIColor(red: 99/255, green: 99/255, blue: 99/255, alpha: 1.0)
      //  lblUserName?.font = UIFont.boldSystemFont(ofSize: 18)
        
        lblUserName?.font = UIFont.systemFont(ofSize: 16)
      //  lblUserName.text = (userDetails["name"]! as! String)
      //  lbl_email.text = (userDetails["email"]! as! String)
        
        
        let defaults = UserDefaults.standard
               lblUserName.text = "\(defaults.string(forKey: "name")!)"
               lbl_email.text = "\(defaults.string(forKey: "email")!)"
        lbl_email?.font = UIFont.systemFont(ofSize: 14)

        
        self.view.backgroundColor = UIColor(red: 254/255, green: 254/255, blue: 254/255, alpha: 1.0)
        self.tableView.backgroundColor = UIColor(red: 254/255, green: 254/255, blue: 254/255, alpha: 1.0)
        self.topView.backgroundColor = UIColor(red: 254/255, green: 254/255, blue: 254/255, alpha: 1.0)
        self.tableView.separatorColor = UIColor(red: 224/255, green: 224/255, blue: 224/255, alpha: 1.0)
        self.tableView.isScrollEnabled = true
        let homeVC = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: ViewControllerIdentifiers.CHEERS_HOME_VC) as? HomeVC
        self.homeViewController = UINavigationController(rootViewController: homeVC!)
        
        let settingsVC = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: ViewControllerIdentifiers.CHEERS_SETTINGS_VC) as? SettingsVC
        self.settingsController = UINavigationController(rootViewController: settingsVC!)
        
        let profileVC = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: ViewControllerIdentifiers.CHEERS_PROFILE_VC) as? ProfileVC
        self.yourProfileViewController = UINavigationController(rootViewController: profileVC!)
        
        let homeVc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: ViewControllerIdentifiers.CHEERS_HOME_VC) as? HomeVC
        self.homeViewController = UINavigationController(rootViewController: homeVc!)
        
      //  let myOrderVC = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: ViewControllerIdentifiers.CHEERS_ORDERDETAILS_VC) as? OrderDetails
        
         let myOrderVC = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "MyOrderVC") as? MyOrderVC
        
        self.orderDeatilsController = UINavigationController(rootViewController: myOrderVC!)
        
        let favVC = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "FavrateVC") as? FavrateVC
        self.favController = UINavigationController(rootViewController: favVC!)
        
        let allCatVc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "AllCategoriesVC") as? AllCategoriesVC
        self.categoryController = UINavigationController(rootViewController: allCatVc!)
        
        let cartVC = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CartVC") as? CartVC
        self.cartViewController = UINavigationController(rootViewController: cartVC!)
        
        self.tableView.registerCellClass(BaseTableViewCell.self)
        self.imageHeaderView = ImageHeaderView.loadNib()
        self.imageHeaderView.isUserInteractionEnabled = false
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let defaults = UserDefaults.standard
        let username = defaults.string(forKey: "userName")
        var temp1 : String!
        temp1 = username
        
        
                      lblUserName.text = "\(defaults.string(forKey: "name")!)"
                      lbl_email.text = "\(defaults.string(forKey: "email")!)"
               lbl_email?.font = UIFont.systemFont(ofSize: 14)
        
        //        if username == nil {
        //            lblUserName.text = ""
        //        }else{
        //            lblUserName.text = temp1!
        //        }
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        self.imageHeaderView = ImageHeaderView.loadNib()
        self.view_header.addSubview(self.imageHeaderView)
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.imageHeaderView.frame = CGRect(x: 0, y: 0, width: self.view_header.frame.width, height: 190)
        self.view.layoutIfNeeded()
    }
    
    func changeViewController(_ menu: LeftMenu)
    {
        switch menu
        {
        case .home:
        let homeVC = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "HomeVC") as? HomeVC
        self.homeViewController = UINavigationController(rootViewController: homeVC!)
            self.slideMenuController()?.changeMainViewController(self.homeViewController, close: true)
        case  .myCart:
            
            
            if let cartcount =  UserDefaults.standard.integer(forKey: "cartCount") as? Int {
                
                if cartcount == 0 {
                    
                    self.slideMenuController()?.changeMainViewController(self.homeViewController, close: true)

                    
                }else {
                    
                    self.slideMenuController()?.changeMainViewController(self.cartViewController, close: true)

                }
                
            }
            
        case  .favrate:
            self.slideMenuController()?.changeMainViewController(self.favController, close: true)
        case  .myorders:
            self.slideMenuController()?.changeMainViewController(self.orderDeatilsController, close: true)
        case  .settings:
            self.slideMenuController()?.changeMainViewController(self.settingsController, close: true)
        case .logOut:
            logOutPopUp()
        }
    }
    
    func logOutApi(){
        Utils.showSpinner()
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        let parameters: [String: Any] = [
            "token": "afe111bd519f0b59930e22b102cec429fb255c0c027ff617e54faf98a343064c",
            "user_id": userID

        ]
        
        Alamofire.request(BASE_URL+"logout", method: .post, parameters: parameters, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            Utils.hideSpinner()
            if response.result.isSuccess {
                
                let resJson = JSON(response.result.value!)
                if resJson["error"]  == false {
                    UserDefaults.standard.set(false, forKey: "isLogin")
                    self.gotoLoginPage()
                }
                else {
                    Utils.alert(message: resJson["message"].string!)
                }
                if response.result.isFailure {
                    let error : Error = response.result.error!
                    Utils.alert(message: error.localizedDescription)
                }
            }
        }
    }
    
    
    func logOutPopUp()
    {
        let alert = UIAlertController(title: "Logout", message: "Are you sure you want to Logout ?", preferredStyle: UIAlertController.Style.alert)
        self.present(alert, animated: true, completion: nil)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.destructive, handler: { action in
            self.logOutApi()
        }))
        alert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.destructive, handler: { action in
        }))
    }
    
    func gotoLoginPage()
    {
        let vc = UIStoryboard.init(name: main, bundle: Bundle.main).instantiateViewController(withIdentifier: ViewControllerIdentifiers.CHEERS_LOGIN_VC) as? ViewController
        self.navigationController?.pushViewController(vc!, animated: true)
        
        //        for vc in (self.navigationController?.viewControllers ?? []) {
        //            if vc is ViewController
        //            {
        //                _ = self.navigationController?.popToViewController(vc, animated: true)
        //                break
        //            }
        //        }
    }
}

extension LeftViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return BaseTableViewCell.height()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let menu = LeftMenu(rawValue: indexPath.row)
        {
            selectedMenuIndex = indexPath.row
            switch menu
            {
            case .home, .myCart, .favrate, .myorders,.settings,.logOut:
                let cell = BaseTableViewCell(style: UITableViewCell.CellStyle.subtitle, reuseIdentifier: BaseTableViewCell.identifier)
                cell.setData(menus[indexPath.row])
                if(indexPath.row != 0)
                {
                    tableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
                }
            }
            self.changeViewController(menu)
        }
    }
}

extension LeftViewController : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return menus.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if let menu = LeftMenu(rawValue: indexPath.row)
        {
            let cell = BaseTableViewCell(style: UITableViewCell.CellStyle.subtitle, reuseIdentifier: BaseTableViewCell.identifier)
            cell.textLabel1?.textColor = UIColor(red: 99/255, green: 99/255, blue: 99/255, alpha: 1.0)
            cell.textLabel1?.font = UIFont.systemFont(ofSize: 16)
            cell.textLabel1.text =  menus[indexPath.row]
            cell.textLabel1.textAlignment = .left
            let img: UIImage = UIImage(named: imagesArray[indexPath.row])!
            cell.imageView1.image = img
            return cell
        }
        return UITableViewCell()
    }
}



