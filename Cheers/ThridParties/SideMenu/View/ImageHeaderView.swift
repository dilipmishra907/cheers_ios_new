//
//  ImageHeaderCell.swift
//  SlideMenuControllerSwift
//
//  Created by Yuji Hato on 11/3/15.
//  Copyright © 2015 Yuji Hato. All rights reserved.
//

import UIKit


class ImageHeaderView : UIView
{
    @IBOutlet weak var profileImage : UIImageView!
    @IBOutlet weak var backgroundImage : UIImageView!
    @IBOutlet weak var lblProfileName: UILabel!
    @IBOutlet weak var viewbg:UIView!
    @IBOutlet var btn_enterprisesList: UIButton!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
       // let mp=CoreData.getProfileData()
        self.viewbg.layoutIfNeeded()
        self.viewbg.layer.cornerRadius = self.viewbg.bounds.size.height / 2
        self.viewbg.clipsToBounds = true
        self.viewbg.layer.borderWidth = 1.5
        self.viewbg.layer.borderColor = UIColor(red: 220.0/255.0, green: 20.0/255.0, blue: 49.0/255.0, alpha: 1.0).cgColor
        
        
      //  profileImage?.image =  #imageLiteral(resourceName: "default_user")

        
       
      //  self.backgroundColor = UIColor.red
        self.profileImage.layoutIfNeeded()
        self.profileImage.layer.cornerRadius = self.profileImage.bounds.size.height / 2
        self.profileImage.clipsToBounds = true
        self.profileImage.layer.borderWidth = 1
        profileImage.layer.borderWidth = 1.0
        self.profileImage.layer.borderColor = UIColor.white.cgColor
        
        
        
        
    }
    
    @IBAction func btn_enterprisesListAction(_ sender: Any)
    {
        
    }
    
}

