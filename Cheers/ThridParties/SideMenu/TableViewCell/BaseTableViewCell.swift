//
//  BaseTableViewCell.swift
//  SlideMenuControllerSwift
//
//  Created by Yuji Hato on 1/22/15.
//  Copyright (c) 2015 Yuji Hato. All rights reserved.
//
import UIKit

open class BaseTableViewCell : UITableViewCell {
    
    var sideView: UIView!
    var bottomView: UIView!
    var imageView1 = UIImageView()
    var textLabel1: UILabel!
    var firstTimeCellClr = false
    var firstTimeCellClr1 = false
    class var identifier: String { return String.className(self) }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    open override func awakeFromNib() {
        
        
    }
    
    open func setup() {
        
        sideView=UIView(frame: CGRect(x:0, y:17, width:3 , height:45))
        sideView.backgroundColor=UIColor.white
        sideView.layer.cornerRadius=0
        sideView.layer.borderWidth=0
        self.addSubview(sideView)
        
        imageView1 = UIImageView(frame: CGRect(x: 20, y: 30, width: 20, height: 20))
        imageView1.image = UIImage(named: "logout")
        self.addSubview(imageView1)

        
        self.textLabel1 = UILabel()
        self.textLabel1.frame = CGRect(x:60,y:25,width:200,height:30)
        self.textLabel1.textAlignment = .left
        self.textLabel1.font = UIFont.systemFont(ofSize: 14)
        
        

        
        self.textLabel1?.textColor = UIColor.darkGray
        self.textLabel1?.lineBreakMode = .byWordWrapping
        self.textLabel1?.numberOfLines = 0
        self.addSubview(self.textLabel1)
        
       bottomView=UIView(frame: CGRect(x:0, y:8, width:300 , height:0.5))
       bottomView.backgroundColor=UIColor.lightGray
      self.addSubview(bottomView)
        
    }
    
    open class func height() -> CGFloat {
        return 60
    }
    
    open func setData(_ data: Any?) {
     
        if let menuText = data as? String {
            self.textLabel1?.text = menuText
        }
    }
  
    
    override open func setHighlighted(_ highlighted: Bool, animated: Bool) {
        if highlighted {
            self.alpha = 0.4
        } else {
            self.alpha = 1.0
        }
    }
    
    // ignore the default handling
    override open func setSelected(_ selected: Bool, animated: Bool) {
    }
    
}

