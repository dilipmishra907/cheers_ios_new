//
//  CartVC.swift
//  Cheers
//
//  Created by Dilip Mishra on 12/05/20.
//  Copyright © 2020 Dilip Mishra. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SDWebImage


class CartVC: UIViewController,UITableViewDelegate,UITableViewDataSource{
    
    @IBOutlet weak var tbl_cart: UITableView!
    var cart_Array = [MyCartModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        tbl_cart.rowHeight = UITableView.automaticDimension
        tbl_cart.estimatedRowHeight = UITableView.automaticDimension
        //getDefaultAddress()
    }
    
    
    func getDefaultAddress(){
        
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        let parameters: [String: Any] = [
            "user_id": userID,
        ]
        
        Alamofire.request(BASE_URL+"getDefaultAddress", method: .post, parameters: parameters, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            if response.result.isSuccess {
                
                let resJson = JSON(response.result.value!)
                if resJson["error"]  == false {
                    
                    
                    
                    if let JSON = response.result.value as? [String: Any] {
                        
                        if let addess =  JSON["address"] as? [String: Any] {
                            defaultAddressDic = addess
                        }else {
                            defaultAddressDic.removeAll()
                        }
                        
                    }
                    
                }
                else {
                    Utils.alert(message: resJson["message"].string!)
                }
                if response.result.isFailure {
                    let error : Error = response.result.error!
                    Utils.alert(message: error.localizedDescription)
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        getDefaultAddress()
        getCartDetailsApi()
        
    }
    
    @IBAction func delete_cart_btn_action(_ sender: UIButton) {
        
        let cellctItem = sender.superview?.superview?.superview as? Cellcart
        let alert = UIAlertController(title: "Delete", message: "Are you sure you want to Delete ?", preferredStyle: UIAlertController.Style.alert)
        self.present(alert, animated: true, completion: nil)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.destructive, handler: { action in
            
            self.deleteCartApi(productID: self.cart_Array[sender.tag].cart_product_Id!, productQty: (cellctItem?.btn_product_qty.titleLabel?.text!)!,packID: self.cart_Array[sender.tag].pack_id!)
            
        }))
        
        alert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.destructive, handler: { action in
        }))
    }
    
    @IBAction func minus_btn_action(_ sender: UIButton) {
        
        let cellctItem = sender.superview?.superview?.superview?.superview as? Cellcart
        
        
        if self.cart_Array[sender.tag].cart_product_quantity != "0" {
            
            var qty:Int = Int(self.cart_Array[sender.tag].cart_product_quantity!)!
            qty = qty - 1
            self.cart_Array[sender.tag].cart_product_quantity! = String(qty)
            cellctItem!.btn_product_qty.setTitle("\(qty)", for: .normal)
            self.addToCartApi(productID: self.cart_Array[sender.tag].cart_product_Id!, packID: self.cart_Array[sender.tag].pack_id!, cartType: "minus")
            
            if self.cart_Array[sender.tag].cart_product_quantity == "0" {
                self.cart_Array.remove(at: sender.tag)
                tbl_cart.reloadData()
            }
        }
        else {
            self.cart_Array.remove(at: sender.tag)
            tbl_cart.reloadData()
        }
        
        UserDefaults.standard.set(self.cart_Array.count, forKey: "cartCount")
        
    }
    
    @IBAction func plus_btn_action(_ sender: UIButton) {
        
        
        let cellctItem = sender.superview?.superview?.superview?.superview as? Cellcart
        var qty:Int = Int(self.cart_Array[sender.tag].cart_product_quantity!)!
        qty = qty + 1
        self.cart_Array[sender.tag].cart_product_quantity! = String(qty)
        cellctItem!.btn_product_qty.setTitle("\(qty)", for: .normal)
        self.addToCartApi(productID: self.cart_Array[sender.tag].cart_product_Id!, packID: self.cart_Array[sender.tag].pack_id!, cartType: "plus")
        
        UserDefaults.standard.set(self.cart_Array.count, forKey: "cartCount")
        
        
    }
    
    
    func addToCartApi(productID : String,packID : String,cartType : String){
        
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        let parameters: [String: Any] = [
            "product_id": productID,
            "flag": cartType,
            "product_quantity": "1",
            "user_id": userID,
            "pack_id": packID,
        ]
        
        Alamofire.request(BASE_URL+"addToCart", method: .post, parameters: parameters, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            Utils.hideSpinner()
            
            switch response.result{
            case .success(let json):
                let resJson = JSON(response.result.value!)
                
                if resJson["error"]  == false {
                    
                    
                    
                }
                else {
                    Utils.alert(message: resJson["message"].string!)
                }
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func deleteCartApi(productID:String,productQty : String,packID : String){
        
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        let parameters: [String: Any] = [
            "user_id": userID,
            "product_id": productID,
            "pack_id": packID,
            "product_quantity": productQty,
        ]
        
        
        Alamofire.request(BASE_URL+"deleteCartProduct", method: .post, parameters: parameters, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            Utils.hideSpinner()
            
            switch response.result{
            case .success(let json):
                let resJson = JSON(response.result.value!)
                if resJson["error"]  == false {
                    if let JSON = response.result.value as? [String: Any] {
                        
                        self.getCartDetailsApi()
                    }
                }
                else {
                    Utils.alert(message: resJson["message"].string!)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    
    @IBAction func home_btn_action(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: ViewControllerIdentifiers.CHEERS_HOME_VC) as! HomeVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cart_Array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : Cellcart?
        cell = (tableView.dequeueReusableCell(withIdentifier: "Cellcart") as! Cellcart?)!
        if cell == nil {
            cell = (UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "Cellcart") as! Cellcart)
        }
        cell!.product_sec_name.text = cart_Array[indexPath.row].cart_product_brand
        cell!.product_name.text = cart_Array[indexPath.row].cart_product_name
        
        if cart_Array[indexPath.row].cart_product_brand! == "Beers" {
            cell!.product_price.text =  cart_Array[indexPath.row].cart_packPrice! + " AED "
            
        }else {
            cell!.product_price.text =  cart_Array[indexPath.row].cart_product_offer_price! + " AED "
        }
        
        let imagName = "http://cheersonlinesales.com/uploads/" + "\(cart_Array[indexPath.row].cart_product_image!)"
        cell!.btn_product_qty.setTitle(cart_Array[indexPath.row].cart_product_quantity!, for: .normal)
        cell!.delete_cartBtn.tag = indexPath.row
        cell!.plus_Btn.tag = indexPath.row
        cell!.minus_Btn.tag = indexPath.row
        cell!.img_view?.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell!.img_view?.sd_setImage(with:URL(string: imagName) , placeholderImage: nil, options: .highPriority, completed: { (imageNew, error, cache, url) in
            if(error != nil)
            {
                cell!.img_view.contentMode =  .scaleAspectFit
                cell!.img_view?.image = UIImage(named:"bg_img")
            }
            else
            {
                cell!.img_view?.image = imageNew
            }
        })
        let bg_view = cell!.viewWithTag(1)
        bg_view!.layer.masksToBounds = false
        bg_view!.layer.shadowColor = UIColor.lightGray.cgColor
        bg_view!.layer.shadowOffset = CGSize(width: 1, height: 2)
        bg_view!.layer.shadowRadius = 5
        bg_view!.layer.shadowOpacity = 0.5
        cell!.selectionStyle = .none
        return cell!
    }
    
    func getCartDetailsApi(){
        Utils.showSpinner()
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        let parameters: [String: Any] = [
            "user_id": userID
        ]
        
        Alamofire.request(BASE_URL+"myCart", method: .post, parameters: parameters, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            Utils.hideSpinner()
            switch response.result{
            case .success(let json):
                let resJson = JSON(response.result.value!)
                
                print("jsonCart",json)
                
                if resJson["error"]  == false {
                    if let JSON = response.result.value as? [String: Any] {
                        var dataArray = [[String:Any]]()
                        dataArray = JSON["data"] as! [[String: Any]]
                        self.cart_Array = parser.JsonToMyCartModal(Array: dataArray)
                        UserDefaults.standard.set(self.cart_Array.count, forKey: "cartCount")
                        
                        self.tbl_cart.reloadData()
                        if self.cart_Array.count == 0 {
                            Utils.alert(message: "No Data Found")
                        }
                    }
                }
                else {
                    Utils.alert(message: resJson["message"].string!)
                }
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    @IBAction func sidemenu_btn_action(_ sender: Any) {
        
        self.slideMenuController()?.openLeft()
        
    }
    @IBAction func buy_btn_action(_ sender: UIButton) {
        
        let vc = UIStoryboard.init(name: main, bundle: Bundle.main).instantiateViewController(withIdentifier: ViewControllerIdentifiers.CHEERS_CHECKOUT_VC) as? CheckoutVC
        vc?.cartArray = cart_Array
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    
}

