//
//  CellAddress.swift
//  Cheers
//
//  Created by Dilip Mishra on 01/05/20.
//  Copyright © 2020 Dilip Mishra. All rights reserved.
//

import UIKit

class CellAddress: UITableViewCell {
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var view_bg: UIView!
    @IBOutlet weak var lbl_landmark: UILabel!
    @IBOutlet weak var lbl_phone_no: UILabel!
    @IBOutlet weak var btn_default_address: UIButton!
    @IBOutlet weak var lbl_addrss_details: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
