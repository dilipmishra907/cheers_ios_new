//
//  AddressVC.swift
//  Cheers
//
//  Created by Dilip Mishra on 28/04/20.
//  Copyright © 2020 Dilip Mishra. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class AddressVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    var addressDetails = [[String:Any]]()
    
    
    
    @IBOutlet weak var tbl_address: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        
        
        //   getAddressBook()
        // Do any additional setup after loading the view.
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated) // No need for semicolon
        
        getAddressBook()
        
        //               if isFromCheckout == true {
        //                   getDefaultAddress()
        //               }
        
        
    }
    
    func getDefaultAddress(){
        
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        let parameters: [String: Any] = [
            "user_id": userID,
        ]
        
        Alamofire.request(BASE_URL+"getDefaultAddress", method: .post, parameters: parameters, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            if response.result.isSuccess {
                
                let resJson = JSON(response.result.value!)
                if resJson["error"]  == false {
                    
                    print("resJsonAD",resJson)
                    
                    
                    if let JSON = response.result.value as? [String: Any] {
                        if let addess =  JSON["address"] as? [String: Any] {
                            defaultAddressDic = addess
                        }
                        
                    }else {
                        defaultAddressDic.removeAll()
                    }
                    
                }
                else {
                    Utils.alert(message: resJson["message"].string!)
                }
                if response.result.isFailure {
                    let error : Error = response.result.error!
                    Utils.alert(message: error.localizedDescription)
                }
            }
        }
    }
    
    
    @IBAction func delete_address_btn_action(_ sender: UIButton) {
        
        let str_addressID = addressDetails[sender.tag]["id"]
        self.deleteAddress(addressID: str_addressID as! String)
    }
    
    
    func deleteAddress(addressID:String){
        
        // Utils.showSpinner()
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        let parameters: [String: Any] = [
            "user_id": userID,
            "address_id": addressID
        ]
        
        Alamofire.request(BASE_URL+"deleteAddressBook", method: .post, parameters: parameters, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            Utils.hideSpinner()
            if response.result.isSuccess {
                
                let resJson = JSON(response.result.value!)
                if resJson["error"]  == false {
                    
                    self.getAddressBook()
                    self.getDefaultAddress()
                    
                    
                }
                else {
                    Utils.alert(message: resJson["message"].string!)
                }
                if response.result.isFailure {
                    let error : Error = response.result.error!
                    Utils.alert(message: error.localizedDescription)
                }
                
            }
        }
    }
    
    @IBAction func back_btn_action(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func home_btn_action(_ sender: Any) {
        
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: ViewControllerIdentifiers.CHEERS_HOME_VC) as! HomeVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.addressDetails.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var celAddress : CellAddress?
        celAddress = (tableView.dequeueReusableCell(withIdentifier: "CellAddress") as! CellAddress?)!
        if celAddress == nil {
            celAddress = (UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "CellCheckout") as! CellAddress)
        }
        
        var str_name = ""
        var str_address = ""
        var str_pincode = ""
        var str_landmark = ""
        
        
        if let strTitle = addressDetails[indexPath.row]["name"] as? String {
            celAddress!.lbl_name.text = strTitle
        }else {
            celAddress!.lbl_name.text = ""
        }
        
        if let address = addressDetails[indexPath.row]["address"] as? String {
            
            celAddress!.lbl_addrss_details.text = address
            
        }else {
            celAddress!.lbl_addrss_details.text = ""
        }
        if let pincode = addressDetails[indexPath.row]["pincode"] as? String {
            // celAddress!.lbl_addrss_details.text = pincode
        }else {
            str_pincode = ""
        }
        if let landmark = addressDetails[indexPath.row]["landmark"] as? String {
            str_landmark = landmark
            
            celAddress!.lbl_landmark.text = landmark
            
            
        }else {
            celAddress!.lbl_landmark.text = ""
        }
        
        if let landmark = addressDetails[indexPath.row]["mobile"] as? String {
            str_landmark = landmark
            
            celAddress!.lbl_phone_no.text = landmark
            
            
        }else {
            celAddress!.lbl_phone_no.text = ""
        }
        
        
        let defaultAddress = addressDetails[indexPath.row]["default_address"]
        if "\(defaultAddress!)" == "0" {
            celAddress!.btn_default_address.backgroundColor =  UIColor.red
        }else {
            celAddress!.btn_default_address.backgroundColor =  UIColor.lightGray
        }
        celAddress!.btn_default_address.tag = indexPath.row
        celAddress?.selectionStyle = .none
        return celAddress!
    }
    
    
    @IBAction func default_btn_address_action(_ sender: UIButton) {
        
        let str_addressID = addressDetails[sender.tag]["id"]
        self.setDefaultAddress(addressID: str_addressID as! String)
    }
    
    @IBAction func add_Address_btn_action(_ sender: Any) {
        
        
        let vc = UIStoryboard.init(name: main, bundle: Bundle.main).instantiateViewController(withIdentifier: ViewControllerIdentifiers.CHEERS_ADDADDRESS_VC) as? AddAddressVC
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        // Swift 4.2 onwards
        return UITableView.automaticDimension
        
    }
    
    
    @IBAction func backBtnAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func getAddressBook(){
        
        Utils.showSpinner()
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        let parameters: [String: Any] = [
            "user_id": userID
        ]
        Alamofire.request(BASE_URL+"getAddressBook", method: .post, parameters: parameters, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            Utils.hideSpinner()
            if response.result.isSuccess {
                
                let resJson = JSON(response.result.value!)
                if resJson["error"]  == false {
                    
                    if let JSON = response.result.value as? [String: Any] {
                        self.addressDetails = JSON["data"] as! [[String: Any]]
                        
                        if self.addressDetails.count == 0 {
                            Utils.alert(message: "No Address Found.")
                            
                        }else {
                            
                        }
                        
                        self.tbl_address.reloadData()
                    }
                }
                else {
                    Utils.alert(message: resJson["message"].string!)
                }
                if response.result.isFailure {
                    let error : Error = response.result.error!
                    Utils.alert(message: error.localizedDescription)
                }
                
            }
        }
    }
    
    
    func setDefaultAddress(addressID:String){
        
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        let parameters: [String: Any] = [
            "user_id": userID,
            "address_id": addressID
        ]
        
        Alamofire.request(BASE_URL+"setDefaultAddress", method: .post, parameters: parameters, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            Utils.hideSpinner()
            if response.result.isSuccess {
                
                let resJson = JSON(response.result.value!)
                if resJson["error"]  == false {
                    
                    self.getAddressBook()
                    
                }
                else {
                    Utils.alert(message: resJson["message"].string!)
                }
                if response.result.isFailure {
                    let error : Error = response.result.error!
                    Utils.alert(message: error.localizedDescription)
                }
                
            }
        }
    }
    
    
    func getDefaultAddress(addressID:String){
        
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        let parameters: [String: Any] = [
            "user_id": userID,
        ]
        
        Alamofire.request(BASE_URL+"getDefaultAddress", method: .post, parameters: parameters, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            Utils.hideSpinner()
            if response.result.isSuccess {
                
                let resJson = JSON(response.result.value!)
                if resJson["error"]  == false {
                    
                    self.getAddressBook()
                    
                }
                else {
                    Utils.alert(message: resJson["message"].string!)
                }
                if response.result.isFailure {
                    let error : Error = response.result.error!
                    Utils.alert(message: error.localizedDescription)
                }
                
            }
        }
    }
}
