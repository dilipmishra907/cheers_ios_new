//
//  UploadDocumentVC.swift
//  Cheers
//
//  Created by Dilip Mishra on 01/05/20.
//  Copyright © 2020 Dilip Mishra. All rights reserved.
//

import UIKit
import Photos


protocol  delegateAfterImageChoose  {
    func sendDataLoginView(temp: [UIImage])
}

class UploadDocumentVC: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    @IBOutlet weak var img_back: UIImageView!
    
    @IBOutlet weak var btn_second_part: UIButton!
    @IBOutlet weak var btn_first_part: UIButton!
    var delegateCustom : delegateAfterImageChoose?
    @IBOutlet weak var img_front: UIImageView!
    
    var firstImg = UIImage()
    var secondImg = UIImage()
    var btnTag = 0
    var idImagesArray: [UIImage] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func upload_document_btn_action(_ sender: Any) {
        
        idImagesArray   = [
            firstImg,secondImg]
        if idImagesArray.count == 2 {
            self.delegateCustom?.sendDataLoginView(temp:idImagesArray)
            self.navigationController?.popViewController(animated: true)
        }else {
            Utils.alert(message: "Please select ID")
            
        }
        
    }
    @IBAction func btn_first_action(_ sender: Any) {
        btnTag = 0
        
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = false
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    @IBAction func btn_secondAction(_ sender: Any) {
        
        btnTag = 1
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = false
            present(imagePicker, animated: true, completion: nil)
        }
        
        
    }
    @IBAction func backBtnAction(_ sender: Any) {
        
        
        //   idImages  += [UIImage(named: "logo.png")!, UIImage(named: "logo2.png")!]
        
        idImagesArray   = [
            firstImg,secondImg]
        
        // self.delegateCustom?.sendDataLoginView(temp:img_front.image!)
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - ImagePickerController Delegate Methods
    //MARK: -
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        // let chosenImage: UIImage? = info[UIImagePickerControllerEditedImage] as! UIImage?
        
        
        if btn_first_part.tag == 1 {
            
        }
        
        // imgMain.image = chosenImage
        picker.dismiss(animated: true, completion: {() -> Void in
        })
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    @IBAction func gallery_btn_action(_ sender: Any) {
        
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = false
            present(imagePicker, animated: true, completion: nil)
        }
        
    }
    @IBAction func camera_btn_action(_ sender: Any) {
        
        AVCaptureDevice.requestAccess(for: AVMediaType.video) { response in
            if response
            {
                if UIImagePickerController.isSourceTypeAvailable(.camera){
                    
                    let picker = UIImagePickerController()
                    picker.delegate = self
                    picker.allowsEditing = true
                    picker.sourceType = .camera
                    self.present(picker, animated: true, completion: nil)
                    
                }else {
                    
                    DispatchQueue.main.async {
                        let alert = UIAlertController(title: "Alert",
                                                      message: "Camera is not available.",
                                                      preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
                        self.present(alert, animated: true)
                    }
                }
            }
            else
            {
                self.showPhotosPermissAlert(msg: "Camera access required for capturing photos!")
            }
        }
        
    }
    
    // Alert message Title
    func showPhotosPermissAlert(msg: String)
    {
        let alert = UIAlertController(
            title: "Alert",
            message: msg,
            preferredStyle: UIAlertController.Style.alert
        )
        alert.addAction(UIAlertAction(title: "Allow", style: .cancel, handler: { (alert) -> Void in
            UIApplication.shared.open((NSURL(string: UIApplication.openSettingsURLString)! as URL), options: [:], completionHandler: nil)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        guard let selectedImage = info[.originalImage] as? UIImage else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        
        var imgname = ""
        
        
        if let asset = info[.phAsset] as? PHAsset, let fileName = asset.value(forKey: "filename") as? String {
            imgname = fileName
        } else if let url = info[.imageURL] as? URL {
            imgname = url.lastPathComponent
        }
        
        if btnTag == 0 {
            firstImg = selectedImage
            btn_first_part.setBackgroundImage(selectedImage, for: .normal)
            img_first_name = imgname
        }else {
            secondImg = selectedImage
            btn_second_part.setBackgroundImage(selectedImage, for: .normal)
            image_second_name = imgname
            
        }
        dismiss(animated: true, completion: nil)
    }
    
    
}
