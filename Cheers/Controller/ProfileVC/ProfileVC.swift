//
//  ProfileVC.swift
//  Cheers
//
//  Created by Dilip Mishra on 25/04/20.
//  Copyright © 2020 Dilip Mishra. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ProfileVC: UIViewController,UITableViewDataSource,UITableViewDelegate{
    
    @IBOutlet weak var txtfld_phone: UITextField!
    @IBOutlet weak var txtfld_name: UITextField!
    @IBOutlet weak var txtfld_email: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = true
        let defaults = UserDefaults.standard
        txtfld_phone.text = "\(defaults.string(forKey: "mobile")!)"
        txtfld_name.text = "\(defaults.string(forKey: "name")!)"
        txtfld_email.text = "\(defaults.string(forKey: "email")!)"
        
    }
    @IBAction func side_menu_btnaction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func update_btn_action(_ sender: Any) {
        
        updateProfileApi()
    }
    
    
    
    func updateProfileApi(){
        
        Utils.showSpinner()
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        let parameters: [String: Any] = [
            "user_id": userID,
            "name": txtfld_name.text!,
            "mobile": txtfld_phone.text!
        ]
        
        Alamofire.request(BASE_URL+"updateProfile", method: .post, parameters: parameters, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            Utils.hideSpinner()
            if response.result.isSuccess {
                
                let resJson = JSON(response.result.value!)
                if resJson["error"]  == false {
                    
                    UserDefaults.standard.set(self.txtfld_phone.text, forKey: "mobile")
                    UserDefaults.standard.set(self.txtfld_name.text, forKey: "name")
                    UserDefaults.standard.set(self.txtfld_email.text, forKey: "email")
                    Utils.alert(message: resJson["message"].string!)
                }
                else {
                    Utils.alert(message: resJson["message"].string!)
                }
                if response.result.isFailure {
                    let error : Error = response.result.error!
                    Utils.alert(message: error.localizedDescription)
                }
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell : UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "cell")
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cell")
        }
        cell?.selectionStyle = .none
        return cell!
    }
    
    
}
