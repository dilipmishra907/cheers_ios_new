//
//  NotificationSettingsVC.swift
//  Cheers
//
//  Created by Dilip Mishra on 24/05/20.
//  Copyright © 2020 Dilip Mishra. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire


class NotificationSettingsVC: UIViewController {
    
    @IBOutlet weak var switcH_ordeer_cancelled: UISwitch!
    @IBOutlet weak var switch_order_delivedrd: UISwitch!
    @IBOutlet weak var switcH_order_processing: UISwitch!
    @IBOutlet weak var switch_order_pending: UISwitch!
    @IBOutlet weak var view_notificationBg: UIView!
    
    var swich_order_cancelled = true
    var swich_order_deliverd = true
    var swich_order_processing = true
    var swich_order_pending = true
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        view_notificationBg.layer.masksToBounds = false
        view_notificationBg.layer.shadowColor = UIColor.lightGray.cgColor
        view_notificationBg.layer.shadowOffset = CGSize(width: 1, height: 2)
        view_notificationBg.layer.shadowRadius = 5
        view_notificationBg.layer.shadowOpacity = 0.5
        
        // Do any additional setup after loading the view.
    }
    @IBAction func order_deliverd_action(_ sender: UISwitch) {
        
        
        if sender.isOn {
            swich_order_deliverd = true
            
        }else {
            swich_order_deliverd = false
            
        }
        setNotificationkeyApi()
        self.putColor(UIColor.red, behindSwitch: switch_order_delivedrd)
        
    }
    
    @IBAction func bckBtnAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func order_pending_btn_action(_ sender: UISwitch) {
        
        if sender.isOn {
            swich_order_cancelled = true
            self.putColor(UIColor.systemGreen, behindSwitch: switch_order_pending)
            
        }else {
            swich_order_cancelled = false
            self.putColor(UIColor.red, behindSwitch: switch_order_pending)
            
        }
        
        setNotificationkeyApi()
        
    }
    @IBAction func order_cancelled_action(_ sender: UISwitch) {
        
        if sender.isOn {
            swich_order_cancelled = true
            //  self.putColor(UIColor.systemGreen, behindSwitch: switch_order_pending)
            
        }else {
            swich_order_cancelled = false
            // self.putColor(UIColor.red, behindSwitch: switch_order_pending)
            
        }
        
        setNotificationkeyApi()
        self.putColor(UIColor.red, behindSwitch: switcH_ordeer_cancelled)
        
        
    }
    
    @IBAction func order_processing_btn_action(_ sender: UISwitch) {
        
        if sender.isOn {
            swich_order_processing = true
            //  self.putColor(UIColor.systemGreen, behindSwitch: switch_order_pending)
            
        }else {
            swich_order_processing = false
            // self.putColor(UIColor.red, behindSwitch: switch_order_pending)
            
        }
        self.putColor(UIColor.red, behindSwitch: switcH_order_processing)
        setNotificationkeyApi()
    }
    
    func putColor(_ color: UIColor, behindSwitch sw: UISwitch) {
        guard sw.superview != nil else {return}
        let onswitch = UISwitch()
        onswitch.isOn = true
        let r = UIGraphicsImageRenderer(bounds:sw.bounds)
        let im = r.image { ctx in
            onswitch.layer.render(in: ctx.cgContext)
        }.withRenderingMode(.alwaysTemplate)
        let iv = UIImageView(image:im)
        iv.tintColor = color
        sw.superview!.insertSubview(iv, belowSubview: sw)
        iv.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            iv.topAnchor.constraint(equalTo: sw.topAnchor),
            iv.bottomAnchor.constraint(equalTo: sw.bottomAnchor),
            iv.leadingAnchor.constraint(equalTo: sw.leadingAnchor),
            iv.trailingAnchor.constraint(equalTo: sw.trailingAnchor),
        ])
    }
    func setNotificationkeyApi(){
        
        Utils.showSpinner()
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        let parameters: [String: Any] = [
            "pending": swich_order_pending,
            "processing": swich_order_processing,
            "cancelled": swich_order_cancelled,
            "delivered": swich_order_deliverd,
            "address_id": "",
            "user_id": userID,
        ]
        
        
        
        
        
        
        Alamofire.request(BASE_URL+"SetNotificationKeys", method: .post, parameters: parameters, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            
            
            Utils.hideSpinner()
            if response.result.isSuccess {
                
                let resJson = JSON(response.result.value!)
                if resJson["error"]  == 0 {
                    Utils.alert(message: resJson["message"].string!)
                    
                    
                }
                else {
                    
                    
                    Utils.alert(message: resJson["message"].string!)
                    
                }
                if response.result.isFailure {
                    let error : Error = response.result.error!
                    Utils.alert(message: error.localizedDescription)
                }
                
            }
        }
    }
}

