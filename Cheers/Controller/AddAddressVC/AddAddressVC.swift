//
//  AddAddressVC.swift
//  Cheers
//
//  Created by Dilip Mishra on 12/05/20.
//  Copyright © 2020 Dilip Mishra. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import GoogleMaps
import MapKit
import CoreLocation


class AddAddressVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,GMSMapViewDelegate,CLLocationManagerDelegate , MKMapViewDelegate {
    

    
    @IBOutlet weak var location_logo: UIImageView!
    @IBOutlet weak var map_view: GMSMapView!
    @IBOutlet weak var txtfld_phone: UITextField!
    @IBOutlet weak var txtfild_name: UITextField!
    @IBOutlet weak var txtfld_landmark: UITextField!
    @IBOutlet weak var txtfld_pincode: UITextField!
    @IBOutlet weak var tbl_address: UITableView!
    @IBOutlet weak var txtfld_address: UITextField!
    
    let locationManager = CLLocationManager()
    @IBOutlet private var mapView: MKMapView!

    @IBOutlet weak var sexond_address: UILabel!
    @IBOutlet weak var first_addess: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        txtfld_phone.text = "9056244"
//        txtfild_name.text = "9056244"
//        txtfld_landmark.text = "9056244"
//        txtfld_address.text = "9056244"

        
        // Set initial location in Honolulu
        let initialLocation = CLLocation(latitude: 21.282778, longitude: -157.829444)
        
        
        

        let oahuCenter = CLLocation(latitude: user_latitude!, longitude: user_longitude!)
        let region = MKCoordinateRegion(
          center: oahuCenter.coordinate,
          latitudinalMeters: 50000,
          longitudinalMeters: 60000)
          mapView.setCameraBoundary(
          MKMapView.CameraBoundary(coordinateRegion: region),
          animated: true)
        
        let zoomRange = MKMapView.CameraZoomRange(maxCenterCoordinateDistance: 200000)
        mapView.setCameraZoomRange(zoomRange, animated: true)
        
              self.locationManager.requestAlwaysAuthorization()
               self.locationManager.requestWhenInUseAuthorization()

               if CLLocationManager.locationServicesEnabled() {
                   locationManager.delegate = self
                   locationManager.desiredAccuracy = kCLLocationAccuracyBest
                   locationManager.startUpdatingLocation()
               }
               mapView.delegate = self
               mapView.mapType = .standard
               mapView.isZoomEnabled = true
               mapView.isScrollEnabled = true

               if let coor = mapView.userLocation.location?.coordinate{
                   mapView.setCenter(coor, animated: true)
               }
        

    }
    
    
    func getAddressFromLatLon(lLatitude: Double, withLongitude longitude: Double)
           {
               var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
               let lat: Double = Double("\(lLatitude)")!
               let lon: Double = Double("\(longitude)")!
               let ceo: CLGeocoder = CLGeocoder()
               center.latitude = lat
               center.longitude = lon
               
               let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
               
               ceo.reverseGeocodeLocation(loc, completionHandler:
                   {(placemarks, error) in
                       
                       
                       
                       
                       if (error != nil)
                       {
                           print("reverse geodcode fail: \(error!.localizedDescription)")
                       }
                       else
                       {
                       
                       let pm = placemarks! as [CLPlacemark]
                       
                       if pm.count > 0 {
                           let pm = placemarks![0]
                           
                           var addressString : String = ""
                           if pm.subLocality != nil {
                            
                            print("pm.subLocality",pm.subLocality)
                               // addressString = addressString + pm.subLocality! + ", "
                           }
                           if pm.thoroughfare != nil {
                            
                            print("pm.subLocality",pm.subLocality)

                               //addressString = addressString + pm.thoroughfare! + ", "
                           }
                           if pm.locality != nil {
                            
                            print("pm.subLocality",pm.subLocality)

                               // addressString = addressString + pm.locality! + ", "
                           }
                           
                           if pm.subAdministrativeArea != nil
                           {
                               addressString = addressString + pm.subAdministrativeArea! + ", "
                               str_address1 = pm.subAdministrativeArea!

                               
                              // cityString =  pm.subAdministrativeArea!
                               
                           }
                           if pm.administrativeArea != nil
                           {
                               addressString = addressString + pm.administrativeArea! + ", "
                            
                            str_address1 = addressString
                            self.sexond_address.text = str_address1

                            
                            print("pm.addressString",addressString)

                             //  stateString = pm.administrativeArea!
                               
                           }
                           if pm.country != nil {
                               addressString = addressString + pm.country! + " "
                            
                              self.first_addess.text = addressString
                            
                            str_address = addressString
                            
                            print(" pm.country!", pm.country!)
                               //myCountryString =  pm.country!
                           }
                           if pm.postalCode != nil {
                               
                               //pincode =  pm.postalCode!
                               //addressString = addressString + pm.postalCode! + " "
                           }
                           
                         //  myCityString =  addressString//
                       }
                       }
               })
       }
    

    @IBAction func chnage_btn_action(_ sender: Any) {
        
        isChnageAddress = true
        let vc = UIStoryboard.init(name: main, bundle: Bundle.main).instantiateViewController(withIdentifier: "SelectCityVC") as? SelectCityVC
           self.navigationController?.pushViewController(vc!, animated: true)
        
        
    }
   
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
       
        if str_address == "" {
            first_addess.text = ""
            location_logo.isHidden = false
        }else {
            first_addess.text = str_address
            location_logo.isHidden = false
        }
        
        if str_address1 == "" {
            sexond_address.text = str_address
            
        }else {
            sexond_address.text = str_address1
        }
        
        if isChnageAddress == false {
            self.getAddressFromLatLon(lLatitude: user_latitude!, withLongitude: user_longitude!)
        }
        
        
    }
    
    func getDefaultAddress(){
        
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        let parameters: [String: Any] = [
            "user_id": userID,
        ]
        
        Alamofire.request(BASE_URL+"getDefaultAddress", method: .post, parameters: parameters, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            if response.result.isSuccess {
                
                let resJson = JSON(response.result.value!)
                if resJson["error"]  == false {
                    
                    if let JSON = response.result.value as? [String: Any] {
                        if let addess =  JSON["address"] as? [String: Any] {
                                defaultAddressDic = addess
                            self.navigationController?.popViewController(animated: true)

                        }
                        
                    }else {
                 }
                    
                }
                else {
                    Utils.alert(message: resJson["message"].string!)
                }
                if response.result.isFailure {
                    let error : Error = response.result.error!
                    Utils.alert(message: error.localizedDescription)
                }
            }
        }
    }
    
    
    
   
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell : UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "cell")
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cell")
        }
        
        cell?.selectionStyle = .none
        return cell!
        
    }
    @IBAction func back_btn_action(_ sender: Any) {
        
         isFromCheckout = false
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Validation
    func isValidParameter() -> Bool {
        
        if ((self.txtfild_name.text?.trimText.isEmpty)!) {
            Utils.alert(message: "Enter valid name, please")
            
            return false
        }
        if ((self.txtfld_phone.text?.trimText.isEmpty)!) {
            Utils.alert(message: "Enter valid phone, please")
            
            return false
        }
        
        if ((self.txtfld_phone.text?.trimText.isEmpty)!) && (self.txtfld_phone.text?.count != 10){
                                Utils.alert(message: "Enter valid phone, please")
                  
                                 return false
                             }
        
        if ((self.txtfld_address.text?.trimText.isEmpty)!) {
            Utils.alert(message: "Enter valid Address, please")
            
            return false
        }

        return true
    }
    
    @IBAction func saveBtnAction(_ sender: Any) {
        if isValidParameter() {
            addAddressApi()
        }
    }
    
    func addAddressApi(){
        
        Utils.showSpinner()
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
       
        let parameters: [String: Any] = [
            "name": txtfild_name.text!,
            "landmark": txtfld_landmark.text!,
            "pincode": "123456",
            "mobile": txtfld_phone.text!,
            "address_id": "",
            "user_id": userID,
            "address": txtfld_address.text!,
            "city_address": str_address,
            "city_name": str_address1
        ]
        
        
        Alamofire.request(BASE_URL+"addressBook", method: .post, parameters: parameters, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            Utils.hideSpinner()
            if response.result.isSuccess {
                let resJson = JSON(response.result.value!)
                
                                
                if resJson["error"]  == false {
                    
                    
                                   if isFromCheckout == true {
                                    self.getDefaultAddress()
                                   }else
                                   {
                                    self.navigationController?.popViewController(animated: true)

                    }
                                   
                    
                    
                }
                else {
                                        
                    Utils.alert(message: resJson["message"].string!)
                    
                }
                if response.result.isFailure {
                    let error : Error = response.result.error!
                    Utils.alert(message: error.localizedDescription)
                }
            }
        }
    }
    
    
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last{
            let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
            
            
        }
    }
    
}





extension String {
    var isValidEmail: Bool {
        NSPredicate(format: "SELF MATCHES %@", "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}").evaluate(with: self)
    }
}
