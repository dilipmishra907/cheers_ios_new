//
//  OrderDetails.swift
//  Cheers
//
//  Created by Dilip Mishra on 26/04/20.
//  Copyright © 2020 Dilip Mishra. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import SDWebImage


class OrderDetails: UIViewController ,UITableViewDataSource,UITableViewDelegate{
    @IBOutlet weak var tbl_order_details: UITableView!
    var normalArray = [[String:Any]]()
    var cancelArray = [[String:Any]]()
    var isNormal = false
    var orderID = ""
    var sub_product_Array = [HomeSubProductModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        getMyBookingDetails()
        isNormal = true
    }
    
    func getMyBookingDetails(){
        
        Utils.showSpinner()
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        let parameters: [String: Any] = [
            "user_id": userID,
            "order_id": orderID,

            
        ]
        
        Alamofire.request(BASE_URL+"getBookingDetails", method: .post, parameters: parameters, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            Utils.hideSpinner()
            
            switch response.result{
            case .success(let json):
                let resJson = JSON(response.result.value!)
                
                
                print("resJsonresJsonresJson",resJson)
                
                if resJson["error"]  == false {
                    if let JSON = response.result.value as? [String: Any] {
                        self.normalArray = JSON["data"] as! [[String: Any]]
                        self.tbl_order_details.reloadData()
                    }
                }
                else {
                    Utils.alert(message: resJson["message"].string!)
                }
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    @IBAction func side_menu_btnAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isNormal == true {
            return self.normalArray.count
            
        }else {
            return self.cancelArray.count
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell : UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "CellOrderDetails")
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "CellOrderDetails")
        }
        
        let bg_view = cell!.viewWithTag(1)
        bg_view!.layer.masksToBounds = false
        bg_view!.layer.shadowColor = UIColor.lightGray.cgColor
        bg_view!.layer.shadowOffset = CGSize(width: 1, height: 2)
        bg_view!.layer.shadowRadius = 5
        bg_view!.layer.shadowOpacity = 0.5
        cell?.selectionStyle = .none
        let lbl_booking_date = cell!.viewWithTag(13) as? UILabel
        let lbl_quantity = cell!.viewWithTag(15) as? UILabel
        let lbl_orderID = cell!.viewWithTag(11) as? UILabel
        let lbl_product_name = cell!.viewWithTag(13) as? UILabel
        let lbl_total_price = cell!.viewWithTag(10) as? UILabel
        let imgProduct = cell!.viewWithTag(9) as? UIImageView
        let lbl_brand = cell!.viewWithTag(4) as? UILabel
        var imagName = ""
            let normalArryaDetails = normalArray[indexPath.row]
          //  lbl_booking_date?.text = (normalArryaDetails["booking_date"] as! String)
          //  lbl_quantity?.text = "Quantity " + (normalArryaDetails["quantity"] as! String)
           /// lbl_orderID?.text = "OrderID " + (normalArryaDetails["order_id"] as! String)
         //   lbl_product_name?.text = (normalArryaDetails["booking_date"] as! String)
        
        if let strBrand = normalArryaDetails["booking_date"] as? String {
                                                          lbl_quantity?.text = strBrand
                                                      }else {
                                                          lbl_quantity?.text = "NA"
                                                      }
        
        if let strBrand = normalArryaDetails["quantity"] as? String {
                                                   lbl_quantity?.text = "quantity " + strBrand
                                               }else {
                                                   lbl_quantity?.text = "NA"
                                               }
        
        if let strBrand = normalArryaDetails["order_id"] as? String {
                                            lbl_orderID?.text = "OrderID " + strBrand
                                        }else {
                                            lbl_orderID?.text = "NA"
                                        }
        
      
        
        if let strBrand = normalArryaDetails["TotalPrice"] as? String {
                          lbl_total_price?.text = strBrand
                      }else {
                          lbl_total_price?.text = "NA"
                      }
        
        if let strBrand = normalArryaDetails["brand"] as? String {
                   lbl_brand?.text = strBrand
               }else {
                   lbl_brand?.text = "NA"
               }
        
            imagName = "http://cheersonlinesales.com/uploads/" + "\(normalArryaDetails["image"]!)"
            imgProduct!.sd_imageIndicator = SDWebImageActivityIndicator.gray
            imgProduct?.sd_setImage(with:URL(string: imagName) , placeholderImage: nil, options: .highPriority, completed: { (imageNew, error, cache, url) in
                if(error != nil)
                {
                    imgProduct!.contentMode =  .scaleAspectFit
                    imgProduct!.image = UIImage(named:"bg_img")
                }
                else
                {
                    imgProduct?.image = imageNew
                }
            })
        return cell!
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 150
    }
   
    
    
   
    
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
