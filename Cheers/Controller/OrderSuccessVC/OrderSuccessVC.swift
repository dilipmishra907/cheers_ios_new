//
//  OrderSuccessVC.swift
//  Cheers
//
//  Created by Dilip Mishra on 30/05/20.
//  Copyright © 2020 Dilip Mishra. All rights reserved.
//

import UIKit

class OrderSuccessVC: UIViewController {
    @IBOutlet weak var imgview: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        showGiFImage()


        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func home_btn_action(_ sender: Any) {
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: ViewControllerIdentifiers.CHEERS_HOME_VC) as! HomeVC
                  self.navigationController?.pushViewController(vc, animated: true)
    }
   
    
    
    func showGiFImage() {
        
        imgview.image = UIImage.gif(name: "right_tick2")
        imgview.animationImages = imgview.image?.images
        imgview.animationDuration = imgview.image!.duration
        imgview.animationRepeatCount = 1
        imgview.stopAnimating()
        
        var values = [CGImage]()
        for image in (imgview.image?.images)! {
            values.append(image.cgImage!)
        }
        let animation = CAKeyframeAnimation(keyPath: "contents")
        animation.calculationMode = CAAnimationCalculationMode.discrete
        animation.duration = imgview.image!.duration
        animation.values = values
        animation.repeatCount = 1
        animation.isRemovedOnCompletion = false
        animation.fillMode = CAMediaTimingFillMode.forwards
        imgview.layer.add(animation, forKey: "animation")
        
    }

    
     func animationDidStop(anim: CAAnimation, finished flag: Bool) {
        if flag {
            print("Animation finished")
        }
    }

}
