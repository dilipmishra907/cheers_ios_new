//
//  ViewController.swift
//  Cheers
//
//  Created by Dilip Mishra on 24/04/20.
//  Copyright © 2020 Dilip Mishra. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class ViewController: UIViewController {
    
    var iconClick = true
    
    @IBOutlet weak var txtfld_password: UITextField!
    @IBOutlet weak var email_textfield: UITextField!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = true
        
        //  email_textfield.text = "abhi@gmail.com"
        // txtfld_password.text = "admin"
        
        // email_textfield.text = "ashwanibull18@gmail.com"
        //  txtfld_password.text = "123456"
        
        
        
        // email_textfield.text = "ashwani94sygrace@gmail.com"
        //txtfld_password.text = "556688"
        
        
        if (UserDefaults.standard.bool(forKey: "isLogin"))
        {
            
            goToHomePage()
            
        }
        
        // Do any additional setup after loading the view.
    }
    @IBAction func login_btn_action(_ sender: Any) {
        
    
        loginApi()
      
    }
    
    @IBAction func forgot_password_btnaction(_ sender: Any) {
        
        let vc = UIStoryboard.init(name: main, bundle: Bundle.main).instantiateViewController(withIdentifier: ViewControllerIdentifiers.CHEERS_FORGOTPASSWORD_VC) as? ForgotPasswordVC
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    
    
    
    
    
    func loginApi(){
        
        Utils.showSpinner()
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        if UserDefaults.standard.value(forKey: "device_token")==nil
        {
            UserDefaults.standard.set("iphone_sumulator", forKey: "device_token")
        }
        
        
        let parameters: [String: Any] = [
            "email": email_textfield.text!,
            "password": txtfld_password.text!,
            "fcm_token": "fcm_token",
        ]
        
        
        
        Alamofire.request(BASE_URL+"login", method: .post, parameters: parameters, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            
            
            
            Utils.hideSpinner()
            if response.result.isSuccess {
                
                
                let resJson = JSON(response.result.value!)
                
                if let JSON = response.result.value as? [String: Any] {
                    
                    if resJson["error"]  == false {
                        userDetails = JSON["user_detail"] as! [String : Any]
                        userID = userDetails["id"] as! String
                        UserDefaults.standard.set("\(userDetails["id"]!)", forKey: "UserID")
                        
                        
                        UserDefaults.standard.set("\(userDetails["mobile"]!)", forKey: "mobile")
                        UserDefaults.standard.set("\(userDetails["name"]!)", forKey: "name")
                        UserDefaults.standard.set("\(userDetails["email"]!)", forKey: "email")
                        
                        let data = NSKeyedArchiver.archivedData(withRootObject: userDetails)
                        UserDefaults.standard.set(data, forKey: "LoginDetails")
                        UserDefaults.standard.set(true, forKey: "isLogin")
                        
                        self.goToHomePage()
                    }
                    else {
                        
                        Utils.alert(message: resJson["message"].string!)
                        
                    }
                    
                    
                    
                }
                
                
                if response.result.isFailure {
                    let error : Error = response.result.error!
                    
                    print("error",error)
                    Utils.alert(message: error.localizedDescription)
                }
                
            }
        }
    }
    
    
    //MARK:- Validation
    func isValidParameter() -> Bool {
        
        if ((self.email_textfield.text?.trimText.isEmpty)!) {
            Utils.alert(message: "Enter valid email, please")
            
            return false
        }
        if (self.txtfld_password.text?.trimText.isEmpty)! {
            Utils.alert(message: "Enter password, please")
            return false
        }
        
        return true
    }
    
    
    func  goToHomePage() {
        
        let mainViewController = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: ViewControllerIdentifiers.CHEERS_HOME_VC) as! HomeVC
        let leftViewController = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "LeftViewController") as! LeftViewController
        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
        UINavigationBar.appearance().tintColor =  UIColor(red:0.2, green:0.2, blue:0.2, alpha:1)
        leftViewController.mainViewController = nvc
        let slideMenuController = ExSlideMenuController(mainViewController:nvc, leftMenuViewController: leftViewController )
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        slideMenuController.delegate = mainViewController as SlideMenuControllerDelegate
        self.navigationController?.pushViewController(slideMenuController, animated: true)
        
    }
    @IBAction func view_passowrd_btn_action(_ sender: Any) {
        
        if(iconClick == true) {
            txtfld_password.isSecureTextEntry = false
            //   eveBtn.setImage(UIImage(named: "eye.png"), for: .normal)
            
        } else {
            txtfld_password.isSecureTextEntry = true
            // eveBtn.setImage(UIImage(named: "eyeHide.png"), for: .normal)
        }
        iconClick = !iconClick
        
    }
    
    @IBAction func dont_have_account_btn_action(_ sender: Any) {
        
        let vc = UIStoryboard.init(name: main, bundle: Bundle.main).instantiateViewController(withIdentifier: ViewControllerIdentifiers.CHEERS_REGISTER_VC) as? RegisterVc
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    
}



extension UIView {
    
    @IBInspectable var cornerRadiusV: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable var borderWidthV: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderColorV: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
}
