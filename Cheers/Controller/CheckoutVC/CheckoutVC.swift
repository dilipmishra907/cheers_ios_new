//
//  CheckoutVC.swift
//  Cheers
//
//  Created by Dilip Mishra on 26/04/20.
//  Copyright © 2020 Dilip Mishra. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire
import SwiftyJSON


class CheckoutVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var btn_change_address: UIButton!
    @IBOutlet weak var lbl_delivery_charge_value: UILabel!
    @IBOutlet weak var bgview_address: UIView!
    @IBOutlet weak var bgview: UIView!
    @IBOutlet weak var lbl_subtotal: UILabel!
    @IBOutlet weak var lbl_retail_tax_amount: UILabel!
    @IBOutlet weak var lbl_vat_amount: UILabel!
    @IBOutlet weak var final_total_amount: UILabel!
    @IBOutlet weak var lbl_delivery_charge: UILabel!
    var subtotalVal = 0.0
    var deliveryCharge = 0.0
    var cartArray = [MyCartModel]()
    @IBOutlet weak var tbl_headerView: UIView!
    
    // defaultAddress
    
    @IBOutlet weak var lbl_landmark: UILabel!
    @IBOutlet weak var lbl_address: UILabel!
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var lbl_mobile: UILabel!
    @IBOutlet weak var tbl_checkout: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        

        self.navigationController?.navigationBar.isHidden = true
        bgview.layer.masksToBounds = false
        bgview.layer.shadowColor = UIColor.lightGray.cgColor
        bgview.layer.shadowOffset = CGSize(width: 1, height: 2)
        bgview.layer.shadowRadius = 5
        bgview.layer.shadowOpacity = 0.5
        bgview_address.layer.masksToBounds = false
        bgview_address.layer.shadowColor = UIColor.lightGray.cgColor
        bgview_address.layer.shadowOffset = CGSize(width: 1, height: 2)
        bgview_address.layer.shadowRadius = 5
        bgview_address.layer.shadowOpacity = 0.5
        bgview_address.layer.cornerRadius = 5
        tbl_checkout.rowHeight = UITableView.automaticDimension
        tbl_checkout.estimatedRowHeight = UITableView.automaticDimension
        
        
        finalCartCalculation()
    }
    
    func finalCartCalculation(){
        
        subtotalVal = 0
        subtotalVal = 0
        subtotalVal = 0
        
        
        for i in (0..<cartArray.count) {
          //  let qty:Double? = Double(cartArray[i].cart_packPrice!)
            var qty:Double? = 0.0
            
            if cartArray[i].cart_product_brand! == "Beers" {
                              qty =  Double(cartArray[i].cart_packPrice!)
                          }else {
                              qty =  Double(cartArray[i].cart_product_offer_price!)
                          }
            
            let offerPrice:Double? = Double(cartArray[i].cart_product_quantity!)
            let finalCalculation:Double? = qty! *  offerPrice!
            subtotalVal = subtotalVal + finalCalculation!
        }
        lbl_subtotal.text = "\(String(format:"%.1f", subtotalVal))" + " AED"
        let vatValueAmount = calculatePercentage(value: subtotalVal,percentageVal: 5)
        let retailTaxAmount = calculatePercentage(value: subtotalVal,percentageVal: 30)
        
        
        if subtotalVal >= 300.0 {
                   deliveryCharge = 0
        }else {
            deliveryCharge = 50
        }
        
        let finalAmount = vatValueAmount + retailTaxAmount + subtotalVal + deliveryCharge
        lbl_vat_amount.text = "\(String(format:"%.1f", vatValueAmount))" + " AED"
        lbl_retail_tax_amount.text = "\(String(format:"%.1f", retailTaxAmount))" + " AED"
        final_total_amount.text = "\(String(format:"%.1f", finalAmount))" + " AED"
        lbl_delivery_charge_value.text = "\(deliveryCharge)" + " AED"
    }
    
    func getDefaultAddress(){
              
              let headers = [
                  "Content-Type": "application/x-www-form-urlencoded"
              ]
              let parameters: [String: Any] = [
                  "user_id": userID,
              ]
              
              Alamofire.request(BASE_URL+"getDefaultAddress", method: .post, parameters: parameters, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
                  if response.result.isSuccess {
                      
                      let resJson = JSON(response.result.value!)
                      if resJson["error"]  == false {
                          
                          print("resJsonAD",resJson)
                          
                          
                          if let JSON = response.result.value as? [String: Any] {
                              if let addess =  JSON["address"] as? [String: Any] {
                                      defaultAddressDic = addess
                              }
                              
                          }else {
                           defaultAddressDic.removeAll()
                       }
                          
                      }
                      else {
                          Utils.alert(message: resJson["message"].string!)
                      }
                      if response.result.isFailure {
                          let error : Error = response.result.error!
                          Utils.alert(message: error.localizedDescription)
                      }
                  }
              }
          }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        if defaultAddressDic.count == 0 {
             tbl_checkout.tableHeaderView?.frame.size = CGSize(width: tbl_checkout.frame.width, height: CGFloat(0))
        btn_change_address.isHidden = false

        }else {
            if let headerView = tbl_checkout.tableHeaderView {
                       let height = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
                       var headerFrame = headerView.frame
                       if height != headerFrame.size.height {
                           headerFrame.size.height = height
                           headerView.frame = headerFrame
                           tbl_checkout.tableHeaderView = tbl_headerView
                       }
                   }
        }

        
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        
       // getDefaultAddress()

        
    
        if defaultAddressDic.count == 0 {
            
            
            tbl_checkout.tableHeaderView?.frame.size = CGSize(width: tbl_checkout.frame.width, height: CGFloat(0))
btn_change_address.isHidden = false

        }else {
            btn_change_address.isHidden = true
            
            tbl_checkout.tableHeaderView?.frame.size = CGSize(width: tbl_checkout.frame.width, height: CGFloat(125))

            
           // tbl_checkout.tableHeaderView = tbl_headerView
           // tbl_checkout.tableHeaderView?.frame = CGRect(x: 0 , y: 0, width: tbl_checkout.frame.width, height: 125)
            //tbl_checkout.tableFooterView?.frame = CGRect(x: 0 , y: 0, width: tbl_checkout.frame.width, height: 145)

            
            if let strAddress = defaultAddressDic["address"] as? String
                   {
                       lbl_address.text = strAddress
                   }
                   else
                   {
                       lbl_address.text = "NA"
                   }
                   
                   if let strName = defaultAddressDic["name"] as? String
                   {
                       lbl_name.text = strName
                   }
                   else
                   {
                       lbl_name.text = "NA"
                   }
                   
                   if let strMobile = defaultAddressDic["mobile"] as? String
                   {
                       lbl_mobile.text = strMobile
                   }
                   else
                   {
                       lbl_mobile.text = "NA"
                   }
                   if let strlandmark = defaultAddressDic["landmark"] as? String
                   {
                       lbl_landmark.text = strlandmark
                   }
                   else
                   {
                       lbl_landmark.text = "NA"
                   }
                   
                   if let straddressID = defaultAddressDic["id"] as? String
                   {
                       addressID = straddressID
                   }
                   else
                   {
                       addressID = "0"
                   }

        }
        

        
       
}
    
    @IBAction func delete_cart_btn_action(_ sender: UIButton) {
        
        let cellctItem = sender.superview?.superview?.superview as? CellCheckout
        let alert = UIAlertController(title: "Delete", message: "Are you sure you want to Delete ?", preferredStyle: UIAlertController.Style.alert)
        self.present(alert, animated: true, completion: nil)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.destructive, handler: { action in
            self.deleteCartApi(productID: self.cartArray[sender.tag].cart_product_Id!, productQty: (cellctItem?.btn_product_qty.titleLabel?.text!)!,packID: self.cartArray[sender.tag].pack_id!)
            
        }))
        
        alert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.destructive, handler: { action in
        }))
    }
    
    func deleteCartApi(productID:String,productQty : String,packID : String){
        
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        let parameters: [String: Any] = [
            "user_id": userID,
            "product_id": productID,
            "pack_id": packID,
            "product_quantity": productQty,
        ]
        
        Alamofire.request(BASE_URL+"deleteCartProduct", method: .post, parameters: parameters, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            Utils.hideSpinner()
            
            switch response.result{
            case .success(let json):
                let resJson = JSON(response.result.value!)
                if resJson["error"]  == false {
                    
                    self.navigationController?.popViewController(animated: true)
                }
                else {
                    Utils.alert(message: resJson["message"].string!)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    
    func getCartDetailsApi(){
        Utils.showSpinner()
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        let parameters: [String: Any] = [
            "user_id": userID
        ]
        
        Alamofire.request(BASE_URL+"myCart", method: .post, parameters: parameters, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            Utils.hideSpinner()
            switch response.result{
            case .success(let json):
                let resJson = JSON(response.result.value!)
                
                if resJson["error"]  == false {
                    if let JSON = response.result.value as? [String: Any] {
                        var dataArray = [[String:Any]]()
                        dataArray = JSON["data"] as! [[String: Any]]
                        self.cartArray = parser.JsonToMyCartModal(Array: dataArray)
                        self.tbl_checkout.reloadData()
                        if self.cartArray.count == 0 {
                            Utils.alert(message: "No Data Found")
                        }
                    }
                }
                else {
                    Utils.alert(message: resJson["message"].string!)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    public func calculatePercentage(value:Double,percentageVal:Double)->Double{
        let val = value * percentageVal
        return val / 100.0
    }
    
    @IBAction func minus_btn_action(_ sender: UIButton) {
        
        let cellctItem = sender.superview?.superview?.superview?.superview as? CellCheckout
        if self.cartArray[sender.tag].cart_product_quantity != "0" {
            var qty:Int = Int(self.cartArray[sender.tag].cart_product_quantity!)!
            qty = qty - 1
            self.cartArray[sender.tag].cart_product_quantity! = String(qty)
            cellctItem!.btn_product_qty.setTitle("\(qty)", for: .normal)
            self.addToCartApi(productID: self.cartArray[sender.tag].cart_product_Id!, packID: self.cartArray[sender.tag].pack_id!, cartType: "minus")
            finalCartCalculation()
            if self.cartArray[sender.tag].cart_product_quantity == "0" {
                self.cartArray.remove(at: sender.tag)
                tbl_checkout.reloadData()
            }
            
        }else {
            
            self.cartArray.remove(at: sender.tag)
            tbl_checkout.reloadData()
        }
        
    }
    
    @IBAction func plus_btn_action(_ sender: UIButton) {
        
        let cellctItem = sender.superview?.superview?.superview?.superview as? CellCheckout
        var qty:Int = Int(self.cartArray[sender.tag].cart_product_quantity!)!
        qty = qty + 1
        self.cartArray[sender.tag].cart_product_quantity! = String(qty)
        cellctItem!.btn_product_qty.setTitle("\(qty)", for: .normal)
        self.addToCartApi(productID: self.cartArray[sender.tag].cart_product_Id!, packID: self.cartArray[sender.tag].pack_id!, cartType: "plus")
        finalCartCalculation()
    }
    
    
    func addToCartApi(productID : String,packID : String,cartType : String){
        
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        let parameters: [String: Any] = [
            "product_id": productID,
            "flag": cartType,
            "product_quantity": "1",
            "user_id": userID,
            "pack_id": packID,
        ]
        
        Alamofire.request(BASE_URL+"addToCart", method: .post, parameters: parameters, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            Utils.hideSpinner()
            
            switch response.result{
            case .success(let json):
                let resJson = JSON(response.result.value!)
                if resJson["error"]  == false {
                    
                }
                else {
                    Utils.alert(message: resJson["message"].string!)
                }
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    
    @IBAction func change_address_btnaction(_ sender: Any) {
        
        let vc = UIStoryboard.init(name: main, bundle: Bundle.main).instantiateViewController(withIdentifier:"AddressVC") as? AddressVC
        isFromCheckout = true
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func home_btn_action(_ sender: Any) {
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: ViewControllerIdentifiers.CHEERS_HOME_VC) as! HomeVC
        
        let controller = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "HomeNVC") as! HomeNVC
        
    }
    @IBAction func buy_btn_action(_ sender: Any) {
        
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PayUponDeliveryVC") as! PayUponDeliveryVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cartArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : CellCheckout?
        cell = (tableView.dequeueReusableCell(withIdentifier: "CellCheckout") as! CellCheckout?)!
        if cell == nil {
            cell = (UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "Cellcart") as! CellCheckout)
        }
        cell!.product_sec_name.text = cartArray[indexPath.row].cart_product_brand
        
        if cartArray[indexPath.row].cart_product_brand! == "Beers" {
                   cell!.product_price.text =  cartArray[indexPath.row].cart_packPrice! + " AED "

               }else {
                   cell!.product_price.text =  cartArray[indexPath.row].cart_product_offer_price! + " AED "
               }
        
        cell!.product_name.text = cartArray[indexPath.row].cart_product_name
        let imagName = "http://cheersonlinesales.com/uploads/" + "\(cartArray[indexPath.row].cart_product_image!)"
        cell!.btn_product_qty.setTitle(cartArray[indexPath.row].cart_product_quantity!, for: .normal)
        cell!.delete_cartBtn.tag = indexPath.row
        cell!.plus_Btn.tag = indexPath.row
        cell!.minus_Btn.tag = indexPath.row
        cell!.btn_product_qty.tag = indexPath.row
        
        
        cell!.img_view?.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell!.img_view?.sd_setImage(with:URL(string: imagName) , placeholderImage: nil, options: .highPriority, completed: { (imageNew, error, cache, url) in
            if(error != nil)
            {
                cell!.img_view.contentMode =  .scaleAspectFit
                cell!.img_view?.image = UIImage(named:"bg_img")
            }
            else
            {
                cell!.img_view?.image = imageNew
            }
        })
        let bg_view = cell!.viewWithTag(1)
        bg_view!.layer.masksToBounds = false
        bg_view!.layer.shadowColor = UIColor.lightGray.cgColor
        bg_view!.layer.shadowOffset = CGSize(width: 1, height: 2)
        bg_view!.layer.shadowRadius = 5
        bg_view!.layer.shadowOpacity = 0.5
        cell?.selectionStyle = .none
        return cell!
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
     
    @IBAction func backBtnAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
}
