//
//  CellCheckout.swift
//  Cheers
//
//  Created by Dilip Mishra on 26/04/20.
//  Copyright © 2020 Dilip Mishra. All rights reserved.
//

import UIKit

class CellCheckout: UITableViewCell {
    
       @IBOutlet weak var delete_cartBtn: UIButton!
       @IBOutlet weak var product_name: UILabel!
       @IBOutlet weak var btn_product_qty: UIButton!
       @IBOutlet weak var product_sec_name: UILabel!
       @IBOutlet weak var product_price: UILabel!
       @IBOutlet weak var img_view: UIImageView!
       @IBOutlet weak var plus_Btn: UIButton!
       @IBOutlet weak var minus_Btn: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
