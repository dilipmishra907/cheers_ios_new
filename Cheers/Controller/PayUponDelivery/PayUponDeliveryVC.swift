//
//  PayUponDeliveryVC.swift
//  Cheers
//
//  Created by Dilip Mishra on 30/05/20.
//  Copyright © 2020 Dilip Mishra. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class PayUponDeliveryVC: UIViewController,UITextViewDelegate {
    @IBOutlet weak var txtview: UITextView!
    @IBOutlet weak var btn_cash_on_delivery: UIButton!
    @IBOutlet weak var btn_card_on_delivery: UIButton!
    
    var strDeliveryType = "0"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtview.delegate = self
        txtview.text = "Note"
        // Do any additional setup after loading the view.
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {

        if textView.text == "ANY SPECIFC NOTE REGARDING PAYMENT AND DELIVERY" {
            textView.text = nil
        }
    }

    func textViewDidEndEditing(_ textView: UITextView) {

        if textView.text.isEmpty {
            DispatchQueue.main.async {
                textView.text = "ANY SPECIFC NOTE REGARDING PAYMENT AND DELIVERY"
            }
        }
    }

    
    @IBAction func back_btn_action(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
        
        
    }
    
    @IBAction func btn_action_payon_delivery(_ sender: Any) {
        
        strDeliveryType = "1"
        btn_cash_on_delivery.backgroundColor = UIColor.systemBlue
        btn_card_on_delivery.backgroundColor = UIColor( red: CGFloat(240/255.0), green: CGFloat(240/255.0), blue: CGFloat(240/255.0), alpha: CGFloat(1.0) )
        btn_cash_on_delivery.setTitleColor(UIColor.white, for: .normal)
        btn_card_on_delivery.setTitleColor(UIColor.darkGray, for: .normal)

    }
    
    @IBAction func btn_action_card_on_delivery(_ sender: Any) {
        
        strDeliveryType = "2"

        
        btn_card_on_delivery.backgroundColor = UIColor.systemBlue
        btn_cash_on_delivery.backgroundColor = UIColor( red: CGFloat(240/255.0), green: CGFloat(240/255.0), blue: CGFloat(240/255.0), alpha: CGFloat(1.0) )
        btn_card_on_delivery.setTitleColor(UIColor.white, for: .normal)
        btn_cash_on_delivery.setTitleColor(UIColor.darkGray, for: .normal)
        
    }
    @IBAction func buy_btn_action(_ sender: Any) {
        
        
        if strDeliveryType == "0" {
            Utils.alert(message: "Please choose delivery type.")

        }else {
            
            if txtview.text == "ANY SPECIFC NOTE REGARDING PAYMENT AND DELIVERY" {
                Utils.alert(message: "Please add delivery note.")

            }else {
                
                placeOrderApi()

            }
            

        }

            
    }
    
    func placeOrderApi(){
        
        Utils.showSpinner()
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        let parameters: [String: Any] = [
         "user_id": userID,
         "address_id": addressID,
         "latitude": "8.9979807",
         "longitude": "12.99707878",
         "note": txtview.text
        ]
     
        Alamofire.request(BASE_URL+"PlaceOrder", method: .post, parameters: parameters, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            Utils.hideSpinner()
            if response.result.isSuccess {
                
                let resJson = JSON(response.result.value!)
                if resJson["error"]  == false {
                   UserDefaults.standard.set(0, forKey: "cartCount")
                   let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "OrderSuccessVC") as! OrderSuccessVC
                          self.navigationController?.pushViewController(vc, animated: true)
                }
                else {
                    Utils.alert(message: resJson["message"].string!)
                }
                if response.result.isFailure {
                    let error : Error = response.result.error!
                    Utils.alert(message: error.localizedDescription)
                }
            }
        }
    }
}



extension UITextView {

    private class PlaceholderLabel: UILabel { }

    private var placeholderLabel: PlaceholderLabel {
        if let label = subviews.compactMap( { $0 as? PlaceholderLabel }).first {
            return label
        } else {
            let label = PlaceholderLabel(frame: .zero)
            label.font = font
            addSubview(label)
            return label
        }
    }

    @IBInspectable
    var placeholder: String {
        get {
            return subviews.compactMap( { $0 as? PlaceholderLabel }).first?.text ?? ""
        }
        set {
            let placeholderLabel = self.placeholderLabel
            placeholderLabel.text = newValue
            placeholderLabel.numberOfLines = 0
            let width = frame.width - textContainer.lineFragmentPadding * 2
            let size = placeholderLabel.sizeThatFits(CGSize(width: width, height: .greatestFiniteMagnitude))
            placeholderLabel.frame.size.height = size.height
            placeholderLabel.frame.size.width = width
            placeholderLabel.frame.origin = CGPoint(x: textContainer.lineFragmentPadding, y: textContainerInset.top)

            textStorage.delegate = self
        }
    }

}

extension UITextView: NSTextStorageDelegate {

    public func textStorage(_ textStorage: NSTextStorage, didProcessEditing editedMask: NSTextStorage.EditActions, range editedRange: NSRange, changeInLength delta: Int) {
        if editedMask.contains(.editedCharacters) {
            placeholderLabel.isHidden = !text.isEmpty
        }
    }

}

