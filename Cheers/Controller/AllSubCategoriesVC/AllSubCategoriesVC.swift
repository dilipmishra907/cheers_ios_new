//
//  AllSubCategoriesVC.swift
//  Cheers
//
//  Created by Dilip Mishra on 12/05/20.
//  Copyright © 2020 Dilip Mishra. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire
import SwiftyJSON



class AllSubCategoriesVC: UIViewController {
    
    
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var clt_item: UICollectionView!
    var sub_product_Array = [HomeSubProductModel]()
    
    var sub_catID = ""
    var sub_cat_title = ""
    var isFromHome = false

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        self.navigationController?.navigationBar.isHidden = true
        lbl_title.text =  sub_cat_title

        if isFromHome == true {

        }else {
            sub_cat_title = ""
        }
        
        getAllSubCategoryProduct(categoryID: sub_catID)

    }
    
    
    func getAllSubCategoryProduct(categoryID : String){
        
        Utils.showSpinner()
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        let parameters: [String: Any] = [
            "category_id": categoryID,
            "user_id": userID,
            "type": sub_cat_title,
            "page": 0
        ]
        
        
        Alamofire.request(BASE_URL+"getAllCategoryProduct", method: .post, parameters: parameters, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            Utils.hideSpinner()
            
            switch response.result{
            case .success(let json):
                let resJson = JSON(response.result.value!)
                                
                if resJson["error"]  == false {
                    if let JSON = response.result.value as? [String: Any] {
                        var dataArray = [[String:Any]]()
                        dataArray = JSON["data"] as! [[String: Any]]
                        self.sub_product_Array = parser.JsonToHomeSubProductModal(Array: dataArray)
                        self.clt_item.reloadData()
                        if self.sub_product_Array.count == 0 {
                            Utils.alert(message: "No Data Found")
                        }
                    }
                }
                else if resJson["error"]  == "false" {
                    
                    if let JSON = response.result.value as? [String: Any] {
                                           var dataArray = [[String:Any]]()
                                           dataArray = JSON["data"] as! [[String: Any]]
                                           self.sub_product_Array = parser.JsonToHomeSubProductModal(Array: dataArray)
                                           self.clt_item.reloadData()
                                           if self.sub_product_Array.count == 0 {
                                               Utils.alert(message: "No Data Found")
                                           }
                                       }
                }
                    
                    
                else {
                    
                    Utils.alert(message: "Error")

                    
                   // Utils.alert(message: resJson["message"].string!)
                }
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    
    @IBAction func wish_btn_action(_ sender: UIButton) {
        
        let cellctItem = sender.superview?.superview?.superview as? CellAllCategoryItem
                
         if self.sub_product_Array[sender.tag].favourite_status == "0" {
            cellctItem!.wish_btn.setImage( UIImage.init(named: "favrate"), for: .normal)
            self.sub_product_Array[sender.tag].favourite_status = "1"
            self.addWishListApi(productID: self.sub_product_Array[sender.tag].sub_product_Id!)
        }else {
            cellctItem!.wish_btn.setImage( UIImage.init(named: "Unfav"), for: .normal)
            self.sub_product_Array[sender.tag].favourite_status = "0"
            self.removeWishListApi(productID: self.sub_product_Array[sender.tag].sub_product_Id!)
        }
     
     let indexPath = IndexPath(item: sender.tag, section: 0)
     self.clt_item.reloadItems(at: [indexPath])

    }
    
    func removeWishListApi(productID : String){
             
             let headers = [
                 "Content-Type": "application/x-www-form-urlencoded"
             ]
             let parameters: [String: Any] = [
                 
                 "product_id": productID,
                 "user_id": userID,
             ]
             
             
             Alamofire.request(BASE_URL+"deleteWishList", method: .post, parameters: parameters, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
                 
                 Utils.hideSpinner()
                 if response.result.isSuccess {
                     
                     let resJson = JSON(response.result.value!)
                     if resJson["error"]  == 0 {
                         
                     }
                     else {
                         
                         
                     }
                     if response.result.isFailure {
                         let error : Error = response.result.error!
                         Utils.alert(message: error.localizedDescription)
                     }
                     
                 }
             }
         }
         
         
         func addWishListApi(productID : String){
             
             let headers = [
                 "Content-Type": "application/x-www-form-urlencoded"
             ]
             let parameters: [String: Any] = [
                 
                 "product_id": productID,
                 "user_id": userID,
             ]
                          
             Alamofire.request(BASE_URL+"addMyWishList", method: .post, parameters: parameters, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
                 
                 Utils.hideSpinner()
                 if response.result.isSuccess {
                    
                    
                     
                     let resJson = JSON(response.result.value!)
                    

                     if resJson["error"]  == 0 {
                         
                     }
                     else {
                         
                       //  Utils.alert(message: resJson["message"].string!)
                         
                     }
                     if response.result.isFailure {
                         let error : Error = response.result.error!
                         Utils.alert(message: error.localizedDescription)
                     }
                 }
             }
         }
    
    @IBAction func backBtnAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
}


// MARK: -
// MARK: - Collection view methods
extension  AllSubCategoriesVC : UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout
{
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var identifierText = ""
        identifierText = "CellAllCategoryItem"
        let cellCollection = collectionView.dequeueReusableCell(withReuseIdentifier: identifierText, for:
            indexPath as IndexPath) as! CellAllCategoryItem
        cellCollection.cell_bgview.layer.masksToBounds = false
        cellCollection.cell_bgview.layer.shadowColor = UIColor.lightGray.cgColor
        cellCollection.cell_bgview.layer.shadowOffset = CGSize(width: 1, height: 2)
        cellCollection.cell_bgview.layer.shadowRadius = 5
        cellCollection.cell_bgview.layer.shadowOpacity = 0.5
        cellCollection.lbl_price.text =   sub_product_Array[indexPath.row].sub_product_price! + " AED"
        cellCollection.wish_btn.tag = indexPath.item
        cellCollection.lbl_product_description.text = sub_product_Array[indexPath.row].sub_product_name
        let imagName = "http://cheersonlinesales.com/uploads/" + "\(sub_product_Array[indexPath.row].sub_product_image!)"
        
        
        if sub_product_Array[indexPath.row].favourite_status == "0" {
                       
                       cellCollection.wish_btn.setImage( UIImage.init(named: "favrate"), for: .normal)
                       
                   }else {
                       cellCollection.wish_btn.setImage( UIImage.init(named: "Unfav"), for: .normal)
                   }
        
        cellCollection.image_view?.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cellCollection.image_view?.sd_setImage(with:URL(string: imagName) , placeholderImage: nil, options: .highPriority, completed: { (imageNew, error, cache, url) in
            if(error != nil)
            {
                cellCollection.image_view.contentMode =  .scaleAspectFit
                cellCollection.image_view?.image = UIImage(named:"bg_img")
            }
            else
            {
                cellCollection.image_view?.image = imageNew
            }
        })
                
        return cellCollection
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return sub_product_Array.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width/2-5, height: 209)
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let vc = UIStoryboard.init(name: main, bundle: Bundle.main).instantiateViewController(withIdentifier: ViewControllerIdentifiers.CHEERS_ITEMDESCRIPTION_VC) as? ItemDescription
        vc!.item_description_array = sub_product_Array[indexPath.row]
        self.navigationController?.pushViewController(vc!, animated: true)
    }
}
