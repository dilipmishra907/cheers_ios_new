//
//  ForgotPasswordVC.swift
//  Cheers
//
//  Created by Dilip Mishra on 06/05/20.
//  Copyright © 2020 Dilip Mishra. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class ForgotPasswordVC: UIViewController {
    
    @IBOutlet weak var email_textfield: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    @IBAction func back_btn_action(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func submit_btn_action(_ sender: Any) {
        
        if isValidParameter() {
            forgot_password_api()
        }
    }
    
    
    
    func forgot_password_api(){
        
        Utils.showSpinner()
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        let parameters: [String: Any] = [
            "email": email_textfield.text!
        ]
        
        Alamofire.request(BASE_URL+"forgot", method: .post, parameters: parameters, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            Utils.hideSpinner()
            if response.result.isSuccess {
                let resJson = JSON(response.result.value!)
                if resJson["error"]  == false {
                    self.showAlertAction(title: "Alert", message: resJson["message"].string!)
                }
                else {
                    Utils.alert(message: resJson["message"].string!)
                }
                if response.result.isFailure {
                    let error : Error = response.result.error!
                    Utils.alert(message: error.localizedDescription)
                }
                
            }
        }
    }
    
    
    func showAlertAction(title: String, message: String){
        
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {
            
            (action:UIAlertAction!) in
            
            self.navigationController?.popViewController(animated: true)
            print("Action")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK:- Validation
    
    
    //MARK:- Validation
    func isValidParameter() -> Bool {
        
        if ((self.email_textfield.text?.trimText.isEmpty)!) {
            Utils.alert(message: "Enter valid email, please")
            
            return false
        }
        
        return true
    }
    
}

