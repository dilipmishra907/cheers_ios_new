//
//  MyOrderVC.swift
//  Cheers
//
//  Created by Dilip Mishra on 31/05/20.
//  Copyright © 2020 Dilip Mishra. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import SDWebImage

//MyOrderVC

class MyOrderVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var bg_complete_order: UIView!
    @IBOutlet weak var bg_cancel_order: UIView!
    @IBOutlet weak var tbl_order_details: UITableView!
    
    var normalArray = [[String:Any]]()
    var cancelArray = [[String:Any]]()
    var isNormal = false
    
    
    
    var sub_product_Array = [HomeSubProductModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        bg_complete_order.isHidden = false
        bg_cancel_order.isHidden = true
        isNormal = true
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
                getMyBookingDetails()
        
    }
    
    func getMyBookingDetails(){
        
        Utils.showSpinner()
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        let parameters: [String: Any] = [
            "user_id": userID
        ]
        
        Alamofire.request(BASE_URL+"myBooking", method: .post, parameters: parameters, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            Utils.hideSpinner()
            
            switch response.result{
            case .success(let json):
                let resJson = JSON(response.result.value!)
                
                
                
                if resJson["error"]  == false {
                    if let JSON = response.result.value as? [String: Any] {
                        self.normalArray = JSON["normal"] as! [[String: Any]]
                        
                        if self.normalArray.count == 0 {
                            Utils.alert(message: "No Data found")
                        }
                        
                        self.cancelArray = JSON["cancelled"] as! [[String: Any]]
                        self.tbl_order_details.reloadData()
                    }
                }
                else {
                    Utils.alert(message: resJson["message"].string!)
                }
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    @IBAction func side_menu_btnAction(_ sender: Any) {
        
        self.slideMenuController()?.openLeft()
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isNormal == true {
            return self.normalArray.count
            
        }else {
            return self.cancelArray.count
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell : UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "CellOrderDetails")
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "CellOrderDetails")
        }
        
        let bg_view = cell!.viewWithTag(1)
        bg_view!.layer.masksToBounds = false
        bg_view!.layer.shadowColor = UIColor.lightGray.cgColor
        bg_view!.layer.shadowOffset = CGSize(width: 1, height: 2)
        bg_view!.layer.shadowRadius = 5
        bg_view!.layer.shadowOpacity = 0.5
        cell?.selectionStyle = .none
        let lbl_booking_date = cell!.viewWithTag(13) as? UILabel
        let lbl_border = cell!.viewWithTag(31) as? UILabel

        let lbl_quantity = cell!.viewWithTag(15) as? UILabel
        let lbl_orderID = cell!.viewWithTag(11) as? UILabel
        let lbl_product_name = cell!.viewWithTag(13) as? UILabel
        let lbl_total_price = cell!.viewWithTag(10) as? UILabel
        let imgProduct = cell!.viewWithTag(9) as? UIImageView
        let lbl_brand = cell!.viewWithTag(4) as? UILabel
        var imagName = ""
        if isNormal == true {
            let normalArryaDetails = normalArray[indexPath.row]
            lbl_booking_date?.text = (normalArryaDetails["booking_date"] as! String)
            lbl_quantity?.text = "Order is processing "
            //lbl_quantity?.text = "Quantity " + (normalArryaDetails["quantity"] as! String)
            lbl_border?.backgroundColor = UIColor.systemGreen
            lbl_orderID?.text = "OrderID " + (normalArryaDetails["order_id"] as! String)
            lbl_product_name?.text = (normalArryaDetails["booking_date"] as! String)
            lbl_total_price?.text =  "Total : " + (normalArryaDetails["TotalPrice"] as! String) + " AED"
            lbl_brand?.text = (normalArryaDetails["brand"] as! String)
            imagName = "http://cheersonlinesales.com/uploads/" + "\(normalArryaDetails["image"]!)"
            imgProduct!.sd_imageIndicator = SDWebImageActivityIndicator.gray
            imgProduct?.sd_setImage(with:URL(string: imagName) , placeholderImage: nil, options: .highPriority, completed: { (imageNew, error, cache, url) in
                if(error != nil)
                {
                    imgProduct!.contentMode =  .scaleAspectFit
                    imgProduct!.image = UIImage(named:"bg_img")
                }
                else
                {
                    imgProduct?.image = imageNew
                }
            })
        }
        else {
            let cancelledArrayDetails = cancelArray[indexPath.row]
            lbl_booking_date?.text = (cancelledArrayDetails["booking_date"] as! String)
            lbl_border?.backgroundColor = UIColor.red
            lbl_booking_date?.text = (cancelledArrayDetails["booking_date"] as! String)
            lbl_quantity?.text = "Quantity " + (cancelledArrayDetails["quantity"] as! String)
            lbl_orderID?.text = "OrderID " + (cancelledArrayDetails["order_id"] as! String)
            lbl_product_name?.text = (cancelledArrayDetails["booking_date"] as! String)
            lbl_total_price?.text =  (cancelledArrayDetails["TotalPrice"] as! String)  + " AED"
            lbl_brand?.text = (cancelledArrayDetails["brand"] as! String)
            imagName = "http://cheersonlinesales.com/uploads/" + "\(cancelledArrayDetails["image"]!)"
            imgProduct!.sd_imageIndicator = SDWebImageActivityIndicator.gray
            imgProduct?.sd_setImage(with:URL(string: imagName) , placeholderImage: nil, options: .highPriority, completed: { (imageNew, error, cache, url) in
                if(error != nil)
                {
                    imgProduct!.contentMode =  .scaleAspectFit
                    imgProduct!.image = UIImage(named:"bg_img")
                }
                else
                {
                    imgProduct?.image = imageNew
                }
            })
        }
        return cell!
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: ViewControllerIdentifiers.CHEERS_ORDERDETAILS_VC) as! OrderDetails
        
        if isNormal == true {
            
            let normalArryaDetails = normalArray[indexPath.row]
            vc.orderID = (normalArryaDetails["order_id"] as! String)
            
        }else {
            let cancelledArrayDetails = normalArray[indexPath.row]
            vc.orderID = (cancelledArrayDetails["order_id"] as! String)

        }

        self.navigationController?.pushViewController(vc, animated: true)

        
    }

    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 150
    }
    @IBAction func cancel_order_btn_action(_ sender: Any) {
        bg_complete_order.isHidden = true
        bg_cancel_order.isHidden = false
        isNormal = false
        tbl_order_details.reloadData()
        if cancelArray.count == 0 {
            Utils.alert(message: "No Data found")
        }
    }
    
    
    @IBAction func complete_order_btn_action(_ sender: Any) {
        bg_complete_order.isHidden = false
        bg_cancel_order.isHidden = true
        isNormal = true
        tbl_order_details.reloadData()
        if normalArray.count == 0 {
            Utils.alert(message: "No Data found")
        }
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
