//
//  SelectCityVC.swift
//  Cheers
//
//  Created by Dilip Mishra on 31/05/20.
//  Copyright © 2020 Dilip Mishra. All rights reserved.
//
import UIKit
import MapKit
import SystemConfiguration

    protocol setEditLocationDelegate
    {
        func selctCity(selectedCity : String)
    }
    protocol zipEditCodeDelegate
    {
        func setZipCode(choose : String)
    }
    protocol countryEditDelegate
    {
        func setCountry(choose : String)
    }
    protocol stateEditDelegate
    {
        func setState(state : String)
    }


   class SelectCityVC: UIViewController
    {
    
        var filterResults = [MKLocalSearchCompletion]()
    

        @IBOutlet weak var srchLocation: UISearchBar!
        @IBOutlet weak var tblLocation: UITableView!
        var currentEditLocationDelegate : setEditLocationDelegate?
        var delegateEditZip : zipEditCodeDelegate?
        var delegateEditCountry : countryEditDelegate?
        var stateEditDel : stateEditDelegate?
        var searchCompleter = MKLocalSearchCompleter()
        var searchResults = [MKLocalSearchCompletion]()
        var arrPlaces = NSMutableArray(capacity: 100)
        let operationQueue = OperationQueue()
        var isfromEditProfile: Bool=false
        
        
        
        
        override func viewDidLoad()
        {
            super.viewDidLoad()
            self.title  = "Select City"
            srchLocation.delegate = self
            tblLocation.dataSource = self
            tblLocation.delegate = self
            tblLocation.isHidden = false
            tblLocation.separatorStyle = .none
            
            srchLocation.becomeFirstResponder()
            tblLocation.keyboardDismissMode = .onDrag
            
            // Search bar Customization
            
            let searchTextField: UITextField? = srchLocation.value(forKey: "searchField") as? UITextField
            if searchTextField!.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
                let attributeDict = [NSAttributedString.Key.foregroundColor: UIColor.lightGray]
                if (isfromEditProfile == true)
                {
                    searchTextField!.attributedPlaceholder = NSAttributedString(string: " Search your city", attributes: attributeDict)
                }
                else
                {
                    searchTextField!.attributedPlaceholder = NSAttributedString(string: " Search for address", attributes: attributeDict)
                }
                searchTextField?.font =  UIFont.init(name: "Roboto-Regular", size: 17)
                searchTextField?.leftViewMode = UITextField.ViewMode.never
                searchTextField?.textColor = UIColor(red: 63.0/255, green: 63.0/255, blue: 63.0/255, alpha: 1.0)
                srchLocation.barTintColor = .white
                srchLocation.backgroundImage = UIImage()
                srchLocation.textField?.returnKeyType = .done
            }
            searchCompleter.delegate = self
            // Do any additional setup after loading the view.
        }
        
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
      
        
    }
        override func viewWillDisappear(_ animated: Bool) {
            super.viewWillDisappear(true)
          
        }
        
    
    @IBAction func back_btn_action(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
       
       
        
        
        // ReverseGeocodeLocation for get user current location
        func getAddressFromLatLon(lLatitude: String, withLongitude longitude: String)
        {
            if(isInternetAvailable())
            {
            var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
            let lat: Double = Double("\(lLatitude)")!
            let lon: Double = Double("\(longitude)")!
            let ceo: CLGeocoder = CLGeocoder()
            center.latitude = lat
            center.longitude = lon
            
            let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
            
            ceo.reverseGeocodeLocation(loc, completionHandler:
                {(placemarks, error) in
                    
                    
                    
                    
                    if (error != nil)
                    {
                        print("reverse geodcode fail: \(error!.localizedDescription)")
                    }
                    else
                    {
                    
                    let pm = placemarks! as [CLPlacemark]
                    
                    if pm.count > 0 {
                        let pm = placemarks![0]
                        
                        var addressString : String = ""
                        if pm.subLocality != nil {
                            // addressString = addressString + pm.subLocality! + ", "
                        }
                        if pm.thoroughfare != nil {
                            //addressString = addressString + pm.thoroughfare! + ", "
                        }
                        if pm.locality != nil {
                            // addressString = addressString + pm.locality! + ", "
                        }
                        
                        if pm.subAdministrativeArea != nil
                        {
                            self.stateEditDel?.setState(state: pm.subAdministrativeArea!)
                            addressString = addressString + pm.subAdministrativeArea! + ", "
                           // str_address = pm.subAdministrativeArea!
                            
                            print("str_address1",str_address)

                            
                           // cityString =  pm.subAdministrativeArea!
                            
                        }
                        if pm.administrativeArea != nil
                        {
                            self.stateEditDel?.setState(state: pm.subAdministrativeArea!)
                            addressString = addressString + pm.administrativeArea! + ", "
                          //  stateString = pm.administrativeArea!
                            
                        }
                        if pm.country != nil {
                            addressString = addressString + pm.country! + " "
                            self.delegateEditCountry?.setCountry(choose: pm.country!)
                            //myCountryString =  pm.country!
                        }
                        if pm.postalCode != nil {
                            
                            //pincode =  pm.postalCode!
                            self.delegateEditZip?.setZipCode(choose: pm.postalCode!)
                            //addressString = addressString + pm.postalCode! + " "
                        }
                        
                        self.navigationController?.popViewController(animated:true)
                      //  myCityString =  addressString//
                        self.currentEditLocationDelegate?.selctCity(selectedCity: addressString)
                    }
                    }
            })
            
        }
    }
        
        
        
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
        
        
        
    func isInternetAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }
    
        
    }
    
    // Mark :- SearchBar Delegate Method
    extension SelectCityVC: UISearchBarDelegate {
        func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
            
            
            searchCompleter.queryFragment = searchText

            

        }
        
        
        func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
            searchBar.resignFirstResponder()
        }
        
        
    }
    
    extension SelectCityVC: MKLocalSearchCompleterDelegate {
        
        func completerDidUpdateResults(_ completer: MKLocalSearchCompleter) {
            
            filterResults = completer.results
            searchResults.removeAll()
            
            
            for i in (0..<filterResults.count)
            {
                let Substring = filterResults[i].subtitle
                let Titlestring = filterResults[i].title
                    searchResults.append(filterResults[i])
            }
            
            
            if !(searchResults.count > 0)
            {
               // suggestedlocation_lbl.isHidden = true
              //  location_borderview.isHidden = true
            }
            else
            {
                tblLocation.isHidden = false
              //  suggestedlocation_lbl.isHidden = false
              //  location_borderview.isHidden = false
            }
            
            tblLocation.reloadData()
        }
        
        func completer(_ completer: MKLocalSearchCompleter, didFailWithError error: Error) {
            // handle error
        }
    }
    
    extension SelectCityVC: UITableViewDataSource {
        
        func numberOfSections(in tableView: UITableView) -> Int {
            return 1
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return searchResults.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let searchResult = searchResults[indexPath.row]
            //  let cell = UITableViewCell(style: .subtitle, reuseIdentifier: nil)
            let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "locationCell")! as UITableViewCell
            cell.textLabel?.text = searchResult.title
            cell.textLabel?.font = UIFont.init(name: "Roboto-Regular", size: 13)
            cell.textLabel?.numberOfLines = 0
            cell.textLabel?.textColor = UIColor(red: 72.0/255, green: 72.0/255, blue: 72.0/255, alpha: 1.0)
            return cell
        }
        
    }
    
    
    
    extension SelectCityVC: UITableViewDelegate {
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            tableView.deselectRow(at: indexPath, animated: true)
            
            let searchResult = searchResults[indexPath.row]
            str_address1 = searchResult.title
            str_address = searchResult.subtitle

            let searchRequest = MKLocalSearch.Request(completion: searchResult)
            let search = MKLocalSearch(request: searchRequest)
            search.start { (response, error) in
                
                let coordinate = response?.mapItems[0].placemark.coordinate
                let latStr =  coordinate?.latitude.description
                let longStr =  coordinate?.longitude.description
                
                self.getAddressFromLatLon(lLatitude: latStr!, withLongitude: longStr!)
                //self.currentLocationDelegate?.selctCity(selectedCity: searchResult.title)
                self.navigationController?.popViewController(animated:true)
                self.srchLocation.resignFirstResponder()
                
            }
        }
    }
    
    

    
    
    /// Mark : - ADD Done Button
    extension UISearchBar {
        var textField: UITextField? {
            return getTextField(inViews: subviews)
        }
        
        private func getTextField(inViews views: [UIView]?) -> UITextField? {
            guard let views = views else { return nil }
            
            for view in views {
                if let textField = (view as? UITextField) ?? getTextField(inViews: view.subviews) {
                    return textField
                }
            }
            
            return nil
        }
    }

extension UINavigationBar
{
    func setBottomBorderColor(color: UIColor)
    {
        let navigationSeparator = UIView(frame: CGRect(x: 0, y: self.frame.size.height - 0.5, width: self.frame.size.width, height: 0.5))
        navigationSeparator.backgroundColor = color
        navigationSeparator.isOpaque = true
        navigationSeparator.tag = 123
        if let oldView = self.viewWithTag(123) {
            oldView.removeFromSuperview()
        }
        self.addSubview(navigationSeparator)
        
    }
}


