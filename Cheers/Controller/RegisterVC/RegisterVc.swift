//
//  RegisterVc.swift
//  Cheers
//
//  Created by Dilip Mishra on 24/04/20.
//  Copyright © 2020 Dilip Mishra. All rights reserved.
//

import UIKit
import SafariServices
import FBSDKLoginKit
import FBSDKCoreKit
import Alamofire
import SwiftyJSON



class RegisterVc: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,delegateAfterImageChoose {
    
    @IBOutlet weak var txtfld_document: UITextField!
    @IBOutlet weak var txtfld_phone: UITextField!
    @IBOutlet weak var txtfild_name: UITextField!
    @IBOutlet weak var txtfld_birth_date: UITextField!
    @IBOutlet weak var txtfld_password: UITextField!
    @IBOutlet weak var email_textfield: UITextField!
    var idImagesArray: [UIImage] = []
    var iconClick = true
    
    
    var document_img = UIImage()
    
    @IBOutlet weak var tbl_view: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = true
        tbl_view.backgroundColor = UIColor.white
        txtfld_birth_date.delegate = self
        let todaysDate = Date()
        let datePickerView = UIDatePicker()
        datePickerView.datePickerMode = .date
        txtfld_birth_date.inputView = datePickerView
        datePickerView.maximumDate = todaysDate
        datePickerView.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
    }
    
    
    //MARK:- mange Date picker action
    @objc func handleDatePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        txtfld_birth_date.text = dateFormatter.string(from: sender.date)
    }
    
    
    
    
    func sendDataLoginView(temp : [UIImage])
    {
        idImagesArray = temp
        txtfld_document.text = img_first_name + "," + image_second_name
    }
    
    func registerApi() {
        
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        let parameters: [String : Any] =   ["name": txtfild_name.text!,"email": email_textfield.text!,"password": txtfld_password.text!,"dob": txtfld_birth_date.text!,"mobile": txtfld_phone.text!]
        
        
        Utils.showSpinner()
        
        Alamofire.upload(multipartFormData:
            {
                (multipartFormData) in
                
                
                
                
                for imageData in self.idImagesArray {
                    
                    let compressImg = imageData.resizeWithWidth(width: 100)!
                    let compressData = compressImg.jpegData(compressionQuality: 0.9)
                    let compressedImage = UIImage(data: compressData!)
                    let imageData = compressedImage!.jpegData(compressionQuality: 0.9)
                    multipartFormData.append(imageData!, withName: "passport", fileName: "file.jpeg", mimeType: "image/jpeg")
                    
                }
                
                for (key, value) in parameters
                {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
        }, to:BASE_URL+"register",headers:headers)
        { (result) in
            switch result {
            case .success(let upload,_,_ ):
                upload.uploadProgress(closure: { (progress) in
                    //Print progress
                })
                upload.responseString
                    { response in
                        
                        Utils.hideSpinner()
                        
                        
                        let resJson = JSON(response.result.value!)
                        
                        
                        do{
                            if let json = resJson.string!.data(using: String.Encoding.utf8){
                                if let jsonData = try JSONSerialization.jsonObject(with: json, options: .allowFragments) as? [String:AnyObject]{
                                    
                                    
                                    let error = jsonData["error"] as? Bool
                                    if error  == false {
                                        
                                        let vc = UIStoryboard.init(name: main, bundle: Bundle.main).instantiateViewController(withIdentifier: ViewControllerIdentifiers.CHEERS_CONGRULATIONS_VC) as? CongrulationVC
                                        self.navigationController?.pushViewController(vc!, animated: true)
                                        
                                    }
                                    else {
                                        Utils.alert(message: "Error")
                                    }
                                }
                            }
                        }catch {
                            print(error.localizedDescription)
                            
                        }
                        
                        
                        
                        
                        
                        //                        if resJson["error"]  == false {
                        //
                        //                            let vc = UIStoryboard.init(name: main, bundle: Bundle.main).instantiateViewController(withIdentifier: ViewControllerIdentifiers.CHEERS_CONGRULATIONS_VC) as? CongrulationVC
                        //                                self.navigationController?.pushViewController(vc!, animated: true)
                        //
                        //                        }else {
                        //
                        //                        }
                        
                        print("responseAbc",response)
                        
                }
            case .failure(let encodingError):
                break
            }
        }
        
        
    }
    
    
    
    @IBAction func signup_with_facebook_action(_ sender: Any) {
        
        facebooklogin()
        
    }
    @IBAction func submit_btn_action(_ sender: Any) {
        
        if isValidParameter() {
            view.endEditing(true)
            registerApi()
        }
    }
    
    func facebooklogin() {
        
        let loginManager = LoginManager()
        loginManager.logIn(permissions: [.publicProfile], viewController: nil) { loginResult in
            switch loginResult {
            case .failed(let error):
                print(error)
            case .cancelled:
                print("User cancelled login.")
            case .success( _, _, _):
                print("Logged in!")
                
                self.returnUserData()
            }
        }
    }
    
    func returnUserData() {
        let graphRequest : GraphRequest = GraphRequest(graphPath: "me", parameters: ["fields":"email,name"])
        graphRequest.start(completionHandler: { (connection, result, error) -> Void in
            if ((error) != nil) {
                // Process error
                print("\n\n Error: \(error!)")
            } else {
                let resultDic = result as! NSDictionary
                print("\n\n  fetched user: \(result!)")
                if (resultDic.value(forKey:"name") != nil) {
                    let userName = resultDic.value(forKey:"name")! as! String as NSString?
                    print("\n User Name is: \(userName!)")
                }
                
                if (resultDic.value(forKey:"email") != nil) {
                    let userEmail = resultDic.value(forKey:"email")! as! String as NSString?
                    print("\n User Email is: \(userEmail!)")
                }
            }
        })
    }
    
    
    
    
    @IBAction func alredy_btn_action(_ sender: Any) {
        
        let vc = UIStoryboard.init(name: main, bundle: Bundle.main).instantiateViewController(withIdentifier: ViewControllerIdentifiers.CHEERS_LOGIN_VC) as? ViewController
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    @IBAction func upload_document_btn_action(_ sender: UIButton) {
        
        let vc = UIStoryboard.init(name: main, bundle: Bundle.main).instantiateViewController(withIdentifier: ViewControllerIdentifiers.CHEERS_UPLOADOCUMENT_VC) as? UploadDocumentVC
        vc?.delegateCustom = self
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell : UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "cell")
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cell")
        }
        
        cell?.selectionStyle = .none
        return cell!
        
        
    }
    
    @IBAction func view_passowrd_btn_action(_ sender: Any) {
        
        if(iconClick == true) {
            txtfld_password.isSecureTextEntry = false
            //   eveBtn.setImage(UIImage(named: "eye.png"), for: .normal)
            
        } else {
            txtfld_password.isSecureTextEntry = true
            // eveBtn.setImage(UIImage(named: "eyeHide.png"), for: .normal)
        }
        iconClick = !iconClick
        
    }
    
    //MARK:- Validation
    func isValidParameter() -> Bool {
        
        if ((self.txtfild_name.text?.trimText.isEmpty)!) {
            Utils.alert(message: "Enter valid name, please")
            
            return false
        }
        if ((self.txtfld_phone.text?.trimText.isEmpty)!) && (self.txtfld_phone.text?.count != 10){
            Utils.alert(message: "Enter valid phone, please")
            
            
            return false
        }
        
        if  (self.txtfld_phone.text?.count != 10){
            Utils.alert(message: "Enter valid phone, please")
            
            return false
        }
        
        
        
        if ((self.txtfld_birth_date.text?.trimText.isEmpty)!) {
            Utils.alert(message: "Enter valid birth date, please")
            
            return false
        }
        if ((self.email_textfield.text?.trimText.isEmpty)!) {
            Utils.alert(message: "Enter valid email, please")
            
            return false
        }
        if (self.txtfld_password.text?.trimText.isEmpty)! {
            Utils.alert(message: "Enter password, please")
            return false
        }
        return true
    }
}


extension String {
    var isValidContact: Bool {
        let phoneNumberRegex = "^[6-9]\\d{10}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneNumberRegex)
        let isValidPhone = phoneTest.evaluate(with: self)
        return isValidPhone
    }
}



extension  SFSafariViewController {
    override open var modalPresentationStyle: UIModalPresentationStyle {
        get { return .fullScreen}
        set { super.modalPresentationStyle = newValue }
    }
}


extension UIImage {
    func resizeWithPercent(percentage: CGFloat) -> UIImage? {
        let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: size.width * percentage, height: size.height * percentage)))
        imageView.contentMode = .scaleAspectFit
        imageView.image = self
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.render(in: context)
        guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        UIGraphicsEndImageContext()
        return result
    }
    func resizeWithWidth(width: CGFloat) -> UIImage? {
        let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))))
        imageView.contentMode = .scaleAspectFit
        imageView.image = self
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.render(in: context)
        guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        UIGraphicsEndImageContext()
        return result
    }
}
