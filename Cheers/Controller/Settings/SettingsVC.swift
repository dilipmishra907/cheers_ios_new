//
//  SettingsVC.swift
//  Cheers
//
//  Created by Dilip Mishra on 23/05/20.
//  Copyright © 2020 Dilip Mishra. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SafariServices

class SettingsVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = true
        
        // Do any additional setup after loading the view.
    }
    @IBAction func profile_settings_btn_action(_ sender: Any) {
        
        let vc = UIStoryboard.init(name: main, bundle: Bundle.main).instantiateViewController(withIdentifier: ViewControllerIdentifiers.CHEERS_PROFILE_VC) as? ProfileVC
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    
    @IBAction func notification_action(_ sender: Any) {
        
        let vc = UIStoryboard.init(name: main, bundle: Bundle.main).instantiateViewController(withIdentifier:"NotificationSettingsVC") as? NotificationSettingsVC
        self.navigationController?.pushViewController(vc!, animated: true)
        
        
    }
    @IBAction func btn_address_action(_ sender: Any) {
        
        let vc = UIStoryboard.init(name: main, bundle: Bundle.main).instantiateViewController(withIdentifier:"AddressVC") as? AddressVC
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    @IBAction func side_menu_btn_action(_ sender: Any) {
        
        self.slideMenuController()?.openLeft()
        
    }
    @IBAction func privcy_policy_btnaction(_ sender: Any) {
        
        let svc = SFSafariViewController(url: NSURL(string:  "http://cheersonlinesales.com/home/privacy")! as URL)
        self.present(svc, animated: true, completion: nil)
        
    }
    @IBAction func about_us_action(_ sender: Any) {
        
        let svc = SFSafariViewController(url: NSURL(string:  "http://cheersonlinesales.com/home/about")! as URL)
        self.present(svc, animated: true, completion: nil)
        
    }
    
    
    
    @IBAction func log_outBtnAction(_ sender: Any) {
        
        logOutApi()
    }
    
    func logOutApi(){
        
        Utils.showSpinner()
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        let parameters: [String: Any] = [
            "token": "afe111bd519f0b59930e22b102cec429fb255c0c027ff617e54faf98a343064c"
        ]
        
        Alamofire.request(BASE_URL+"logout", method: .post, parameters: parameters, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            Utils.hideSpinner()
            if response.result.isSuccess {
                
                print("response",response)
                let resJson = JSON(response.result.value!)
                if resJson["error"]  == false {
                    self.gotoLoginPage()
                }
                else {
                    Utils.alert(message: resJson["message"].string!)
                }
                if response.result.isFailure {
                    let error : Error = response.result.error!
                    Utils.alert(message: error.localizedDescription)
                }
                
            }
        }
    }
    
    func logOutPopUp()
    {
        let alert = UIAlertController(title: "Logout", message: "Are you sure you want to Logout ?", preferredStyle: UIAlertController.Style.alert)
        self.present(alert, animated: true, completion: nil)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.destructive, handler: { action in
            
            self.logOutApi()
            
        }))
        
        alert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.destructive, handler: { action in
        }))
        
    }
    
    func gotoLoginPage()
    {
        
        for vc in (self.navigationController?.viewControllers ?? []) {
            if vc is ViewController
            {
                _ = self.navigationController?.popToViewController(vc, animated: true)
                break
            }
        }
        
    }
    
    
}
