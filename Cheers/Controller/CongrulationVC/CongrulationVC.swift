//
//  CongrulationVC.swift
//  Cheers
//
//  Created by Dilip Mishra on 03/05/20.
//  Copyright © 2020 Dilip Mishra. All rights reserved.
//

import UIKit
import SwiftGifOrigin



class CongrulationVC: UIViewController {
    
    @IBOutlet weak var imgview: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        showGiFImage()
        
        
    }
    
    @IBAction func login_btn_action(_ sender: Any) {
        let vc = UIStoryboard.init(name: main, bundle: Bundle.main).instantiateViewController(withIdentifier: ViewControllerIdentifiers.CHEERS_LOGIN_VC) as? ViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    
    func showGiFImage() {
        
        imgview.image = UIImage.gif(name: "right_tick2")
        imgview.animationImages = imgview.image?.images
        imgview.animationDuration = imgview.image!.duration
        imgview.animationRepeatCount = 1
        imgview.stopAnimating()
        
        var values = [CGImage]()
        for image in (imgview.image?.images)! {
            values.append(image.cgImage!)
        }
        let animation = CAKeyframeAnimation(keyPath: "contents")
        animation.calculationMode = CAAnimationCalculationMode.discrete
        animation.duration = imgview.image!.duration
        animation.values = values
        animation.repeatCount = 1
        animation.isRemovedOnCompletion = false
        animation.fillMode = CAMediaTimingFillMode.forwards
        imgview.layer.add(animation, forKey: "animation")
        
    }
    
    
    func animationDidStop(anim: CAAnimation, finished flag: Bool) {
        if flag {
            print("Animation finished")
        }
    }
}
