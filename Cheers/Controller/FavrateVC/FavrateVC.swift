//
//  FavrateVC.swift
//  Cheers
//
//  Created by Dilip Mishra on 23/05/20.
//  Copyright © 2020 Dilip Mishra. All rights reserved.
//


import UIKit
import SDWebImage
import Alamofire
import SwiftyJSON



class FavrateVC: UIViewController {
    
    
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var clt_item: UICollectionView!
    var sub_product_Array = [HomeSubProductModel]()
    
    
    var sub_catID = ""
    var sub_cat_title = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        self.navigationController?.navigationBar.isHidden = true
        lbl_title.text =  "Favarate"
        getMyWishListIem()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        getMyWishListIem()
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: ViewControllerIdentifiers.CHEERS_HOME_VC) as! HomeVC
        
    }
    
    
    @IBAction func wish_btn_action(_ sender: UIButton) {
        
        
        self.sub_product_Array[sender.tag].favourite_status = "0"
        self.removeWishListApi(productID: self.sub_product_Array[sender.tag].sub_product_Id!)
        let indexPath = IndexPath(item: sender.tag, section: 0)
        self.clt_item.reloadItems(at: [indexPath])
        
    }
    
    func removeWishListApi(productID : String){
        
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        let parameters: [String: Any] = [
            
            "product_id": productID,
            "user_id": userID,
        ]
        
        
        Alamofire.request(BASE_URL+"deleteWishList", method: .post, parameters: parameters, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            Utils.hideSpinner()
            if response.result.isSuccess {
                
                
                
                let resJson = JSON(response.result.value!)
                
                
                if resJson["error"]  == false {
                    self.getMyWishListIem()
                    
                }
                else {
                    
                    
                }
                if response.result.isFailure {
                    let error : Error = response.result.error!
                    Utils.alert(message: error.localizedDescription)
                }
                
            }
        }
    }
    
    
    func addWishListApi(productID : String){
        
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        let parameters: [String: Any] = [
            
            "product_id": productID,
            "user_id": userID,
        ]
        
        print("parameters",parameters)
        
        Alamofire.request(BASE_URL+"addMyWishList", method: .post, parameters: parameters, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            Utils.hideSpinner()
            if response.result.isSuccess {
                
                
                
                let resJson = JSON(response.result.value!)
                
                print("resJson",resJson)
                
                if resJson["error"]  == 0 {
                    
                }
                else {
                    
                    self.getMyWishListIem()
                    //  Utils.alert(message: resJson["message"].string!)
                    
                }
                if response.result.isFailure {
                    let error : Error = response.result.error!
                    Utils.alert(message: error.localizedDescription)
                }
            }
        }
    }
    
    @IBAction func sideMenuBtnAction(_ sender: Any) {
        
        
        self.slideMenuController()?.openLeft()
        
    }
    
    
    func getMyWishListIem(){
        
        Utils.showSpinner()
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        let parameters: [String: Any] = [
            "user_id": userID
        ]
        
        Alamofire.request(BASE_URL+"myWishList", method: .post, parameters: parameters, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            Utils.hideSpinner()
            
            switch response.result{
            case .success(let json):
                let resJson = JSON(response.result.value!)
                
                
                
                
                if resJson["error"]  == false {
                    if let JSON = response.result.value as? [String: Any] {
                        var dataArray = [[String:Any]]()
                        dataArray = JSON["wishlist"] as! [[String: Any]]
                        self.sub_product_Array = parser.JsonToHomeSubProductModal(Array: dataArray)
                        self.clt_item.reloadData()
                        if self.sub_product_Array.count == 0 {
                            
                            let alert = UIAlertController(title: "Cheers", message: "No Data Found", preferredStyle: UIAlertController.Style.alert)
                            self.present(alert, animated: true, completion: nil)
                            alert.addAction(UIAlertAction(title: "Oky", style: UIAlertAction.Style.default, handler: { action in
                                
                                
                                
                                self.goToHomePage()
                                
                            }))
                            
                            //Utils.alert(message: "No Data Found")
                        }
                    }
                }
                else {
                    Utils.alert(message: resJson["message"].string!)
                }
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func  goToHomePage() {
        
        let mainViewController = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: ViewControllerIdentifiers.CHEERS_HOME_VC) as! HomeVC
        let leftViewController = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "LeftViewController") as! LeftViewController
        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
        UINavigationBar.appearance().tintColor =  UIColor(red:0.2, green:0.2, blue:0.2, alpha:1)
        leftViewController.mainViewController = nvc
        let slideMenuController = ExSlideMenuController(mainViewController:nvc, leftMenuViewController: leftViewController )
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        slideMenuController.delegate = mainViewController as SlideMenuControllerDelegate
        self.navigationController?.pushViewController(slideMenuController, animated: true)
        
    }
}


// MARK: -
// MARK: - Collection view methods
extension  FavrateVC : UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout
{
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var identifierText = ""
        identifierText = "CellAllCategoryItem"
        let cellCollection = collectionView.dequeueReusableCell(withReuseIdentifier: identifierText, for:
            indexPath as IndexPath) as! CellAllCategoryItem
        cellCollection.cell_bgview.layer.masksToBounds = false
        cellCollection.cell_bgview.layer.shadowColor = UIColor.lightGray.cgColor
        cellCollection.cell_bgview.layer.shadowOffset = CGSize(width: 1, height: 2)
        cellCollection.cell_bgview.layer.shadowRadius = 5
        cellCollection.cell_bgview.layer.shadowOpacity = 0.5
        cellCollection.lbl_price.text =  sub_product_Array[indexPath.row].sub_product_price! + " AED"
        cellCollection.wish_btn.tag = indexPath.item
        cellCollection.lbl_product_description.text = sub_product_Array[indexPath.row].sub_product_name
        let imagName = "http://cheersonlinesales.com/uploads/" + "\(sub_product_Array[indexPath.row].sub_product_image!)"
        cellCollection.wish_btn.setImage( UIImage.init(named: "Unfav"), for: .normal)
        
        cellCollection.image_view?.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cellCollection.image_view?.sd_setImage(with:URL(string: imagName) , placeholderImage: nil, options: .highPriority, completed: { (imageNew, error, cache, url) in
            if(error != nil)
            {
                cellCollection.image_view.contentMode =  .scaleAspectFit
                cellCollection.image_view?.image = UIImage(named:"bg_img")
            }
            else
            {
                cellCollection.image_view?.image = imageNew
            }
            
        })
        
        //  cellCollection.image_view.sd_setImage(with: URL(string:imagName), placeholderImage: nil)
        
        return cellCollection
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        
        return sub_product_Array.count
        
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width/2-5, height: 209)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let vc = UIStoryboard.init(name: main, bundle: Bundle.main).instantiateViewController(withIdentifier: ViewControllerIdentifiers.CHEERS_ITEMDESCRIPTION_VC) as? ItemDescription
        vc!.item_description_array = sub_product_Array[indexPath.row]
        self.navigationController?.pushViewController(vc!, animated: true)
    }
}
