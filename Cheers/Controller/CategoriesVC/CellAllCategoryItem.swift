//
//  CellAllCategoryItem.swift
//  Cheers
//
//  Created by Dilip Mishra on 26/04/20.
//  Copyright © 2020 Dilip Mishra. All rights reserved.
//

import UIKit

class CellAllCategoryItem: UICollectionViewCell {
    @IBOutlet weak var cell_bgview: UIView!
    @IBOutlet weak var lbl_price: UILabel!
    @IBOutlet weak var lbl_product_description: UILabel!
    @IBOutlet weak var lbl_qty: UILabel!
    @IBOutlet weak var image_view: UIImageView!
    @IBOutlet weak var wish_btn: UIButton!
}
