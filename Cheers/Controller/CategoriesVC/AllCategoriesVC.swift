//
//  AllCategoriesVC.swift
//  Cheers
//
//  Created by Dilip Mishra on 25/04/20.
//  Copyright © 2020 Dilip Mishra. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SDWebImage


class AllCategoriesVC: UIViewController {

    @IBOutlet weak var view_catview: UIView!
    @IBOutlet weak var height_catview: NSLayoutConstraint!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var clt_item: UICollectionView!
    @IBOutlet weak var clt_categories: UICollectionView!
    
    var product_array = ProductModel()
    var all_sub_categoriesArray = [AllSubCategoryModel]()
    var catID = ""
    var cat_title = ""


    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        self.navigationController?.navigationBar.isHidden = true
        if isViewAllCategory == true {
            view_catview.isHidden = false
            height_catview.constant = 60
            catID = all_categoriesArray[0].id!
        }else {
            view_catview.isHidden = true
            height_catview.constant = 0
            lbl_title.text = cat_title
        }
        getAllSubCategory(categoryID: catID)
    }
    
    
    func getAllSubCategory(categoryID : String)
    {
        Utils.showSpinner()

        var url:String!
        url = "http://cheersonlinesales.com/api/getAllSubCategory/" + categoryID
            
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default)
            .responseJSON { response in
                
                Utils.hideSpinner()

                
                switch response.result{
                case .success(let json):
                    let resJson = JSON(response.result.value!)
                    if resJson["error"]  == false {
                        if let JSON = response.result.value as? [String: Any] {
                            var dataArray = [[String:Any]]()
                            dataArray = JSON["data"] as! [[String: Any]]
                            self.all_sub_categoriesArray = parser.JsonToAllSubCategoryModal(Array: dataArray)
                            self.clt_item.reloadData()
                                                        
                            if self.all_sub_categoriesArray.count == 0 {
                                Utils.alert(message: "No Data Found")
                            }
                        }
                    }
                    else {
                        Utils.alert(message: resJson["message"].string!)
                    }
                    
                case .failure(let error):
                    print(error)
                }
        }
    }
    
    
  
    
    @IBAction func backBtnAction(_ sender: Any) {
                      
          self.navigationController?.popViewController(animated: true)
       }
    
    
       }


// MARK: -
// MARK: - Collection view methods
extension  AllCategoriesVC : UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout
{
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var identifierText = ""
        if collectionView.tag == 100 {
            identifierText = "Cellcategory"
            let cellCollection = collectionView.dequeueReusableCell(withReuseIdentifier: identifierText, for:
                indexPath as IndexPath) as! Cellcategory
            let lblName = cellCollection.viewWithTag(49) as? UILabel
            lblName!.text = all_categoriesArray[indexPath.item].cataegoryTitle
                  
            if catID == all_categoriesArray[indexPath.item].id {
                cellCollection.shadow_bg_view.backgroundColor = UIColor.red

            }else {
                cellCollection.shadow_bg_view.backgroundColor = UIColor.clear
            }
            
            return cellCollection
        }else {
            identifierText = "CellAllCategoryItem"
            let cellCollection = collectionView.dequeueReusableCell(withReuseIdentifier: identifierText, for:
                indexPath as IndexPath) as! CellAllCategoryItem
            cellCollection.cell_bgview.layer.masksToBounds = false
            cellCollection.cell_bgview.layer.shadowColor = UIColor.lightGray.cgColor
            cellCollection.cell_bgview.layer.shadowOffset = CGSize(width: 1, height: 2)
            cellCollection.cell_bgview.layer.shadowRadius = 5
            cellCollection.cell_bgview.layer.shadowOpacity = 0.5
            cellCollection.lbl_product_description.text = all_sub_categoriesArray[indexPath.row].all_cataegoryTitle
                let imagName = "http://cheersonlinesales.com/uploads/categories/" + "\(all_sub_categoriesArray[indexPath.row].all_image!)"

                cellCollection.image_view?.sd_imageIndicator = SDWebImageActivityIndicator.gray
                cellCollection.image_view?.sd_setImage(with:URL(string: imagName) , placeholderImage: nil, options: .highPriority, completed: { (imageNew, error, cache, url) in
                    if(error != nil)
                    {
                        cellCollection.image_view.contentMode =  .scaleAspectFit
                        cellCollection.image_view?.image = UIImage(named:"bg_img")
                    }
                    else
                    {
                        cellCollection.image_view?.image = imageNew
                    }

                })
                return cellCollection
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView.tag == 100 {
            return all_categoriesArray.count

        }else {
            
            return all_sub_categoriesArray.count
            
        }

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
           if collectionView.tag == 100 {
               let size: CGSize = all_categoriesArray[indexPath.item].cataegoryTitle!.size(withAttributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15.0)])
                         return CGSize(width: size.width+40 , height: 45)
                 }else {
                     return CGSize(width: collectionView.frame.size.width/2-5, height: 190)
                 }
       }
    

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView.tag != 100 {
            
            let vc = UIStoryboard.init(name: main, bundle: Bundle.main).instantiateViewController(withIdentifier: ViewControllerIdentifiers.CHEERS_ALLSUBCAT_VC) as? AllSubCategoriesVC
            vc!.sub_cat_title = all_sub_categoriesArray[indexPath.row].all_cataegoryTitle!
            vc!.sub_catID = all_sub_categoriesArray[indexPath.row].allcat_id!
            vc!.isFromHome = false
            self.navigationController?.pushViewController(vc!, animated: true)
        }
        
        else {
            
            catID = all_categoriesArray[indexPath.item].id!
            clt_categories.reloadData()
            getAllSubCategory(categoryID: "\(catID)")
            
        }
    }
}


extension UIView {

  // OUTPUT 1
  func dropShadow(scale: Bool = true) {
    layer.masksToBounds = false
    layer.shadowColor = UIColor.black.cgColor
    layer.shadowOpacity = 0.5
    layer.shadowOffset = CGSize(width: -1, height: 1)
    layer.shadowRadius = 1

    layer.shadowPath = UIBezierPath(rect: bounds).cgPath
    layer.shouldRasterize = true
    layer.rasterizationScale = scale ? UIScreen.main.scale : 1
  }

  // OUTPUT 2
  func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
    layer.masksToBounds = false
    layer.shadowColor = color.cgColor
    layer.shadowOpacity = opacity
    layer.shadowOffset = offSet
    layer.shadowRadius = radius

    layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
    layer.shouldRasterize = true
    layer.rasterizationScale = scale ? UIScreen.main.scale : 1
  }
}

