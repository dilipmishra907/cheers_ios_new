//
//  HomeVC.swift
//  Cheers
//
//  Created by Dilip Mishra on 24/04/20.
//  Copyright © 2020 Dilip Mishra. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SDWebImage
import UIGradient


class HomeVC: UIViewController,SlideMenuControllerDelegate,UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate {
    
    @IBOutlet weak var lbl_cart_count: UILabel!
    @IBOutlet weak var txtfld_search: UITextField!
    @IBOutlet weak var search_bar: UISearchBar!
    @IBOutlet weak var cat_bg_view: UIView!
    @IBOutlet weak var tblview: UITableView!
    @IBOutlet weak var searchview: UIView!
    @IBOutlet weak var clt_categories: UICollectionView!
    var productsDetails = [String:Any]()
    
    
    //var productDetailModel: [ProductDetailModel] = [ProductDetailModel]()
    
    var productDetailModel = HomeModel()
    var productModel = ProductModel()
    
    var colorArray = [
        GradientPattern(name: "Ocean Blue", fromColors: ["2E3192", "1BFFFF"], gradient: GradientLayer.oceanBlue),
        GradientPattern(name: "Sanguine", fromColors: ["D4145A", "FBB03B"], gradient: GradientLayer.sanguine),
        GradientPattern(name: "Luscious Lime", fromColors: ["009245", "FCEE21"], gradient: GradientLayer.lusciousLime),
        GradientPattern(name: "Purple Lake", fromColors: ["662D8C", "ED1E79"], gradient: GradientLayer.purpleLake),
        GradientPattern(name: "Fresh Papaya", fromColors: ["ED1C24", "FCEE21"], gradient: GradientLayer.freshPapaya),
        GradientPattern(name: "Ultramarine", fromColors: ["00A8C5", "FFFF7E"], gradient: GradientLayer.ultramarine),
        GradientPattern(name: "Pink Sugar", fromColors: ["D74177", "FFE98A"], gradient: GradientLayer.pinkSugar),
        GradientPattern(name: "Deep Sea", fromColors: ["4F00BC", "29ABE2"], gradient: GradientLayer.deepSea),
        GradientPattern(name: "Coastal Breeze", fromColors: ["00B7FF", "FFFFC7"], gradient: GradientLayer.coastalBreeze),
        GradientPattern(name: "Argon", fromColors: ["03001e", "7303c0", "ec38bc", "fdeff9"], gradient: GradientLayer(direction: .custom(77), colors: [UIColor.hex("03001e"), UIColor.hex("7303c0"), UIColor.hex("ec38bc"), UIColor.hex("fdeff9")], locations: [0.0, 0.33, 0.66, 1.0]))
    ]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        self.navigationController?.navigationBar.isHidden = true

        
        if let cartcount =  UserDefaults.standard.integer(forKey: "cartCount") as? Int {
            lbl_cart_count.text =  String(cartcount)
        }else {
            lbl_cart_count.text =  "0"
        }

        
        getHomeData()
    }
    @IBAction func search_btn_action(_ sender: Any) {
        
            let vc = UIStoryboard.init(name: main, bundle: Bundle.main).instantiateViewController(withIdentifier: ViewControllerIdentifiers.CHEERS_SEARCH_VC) as? SearchViewController
        vc!.searchText = txtfld_search.text!
            self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    
    
    func getHomeData(){
        
        Utils.showSpinner()
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        let parameters: [String: Any] = [
            "user_id": userID
        ]
        
        Alamofire.request(BASE_URL+"getHomeData", method: .post, parameters: parameters, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            Utils.hideSpinner()
            if response.result.isSuccess {
                let resJson = JSON(response.result.value!)
                if resJson["error"]  == false {
                    
                    
                    if let JSON = response.result.value as? [String: Any] {
                        self.productsDetails = JSON["data"] as! [String: Any]
                        self.productDetailModel = parser.JsonToHomeModal(dict: self.productsDetails)
                        all_categoriesArray =  self.productDetailModel.categoriesArray
                        self.clt_categories.reloadData()
                        self.tblview.reloadData()
                    }
                }
                else {
                    Utils.alert(message: resJson["message"].string!)
                }
                if response.result.isFailure {
                    let error : Error = response.result.error!
                    Utils.alert(message: error.localizedDescription)
                }
                
            }
        }
        
    }
    
    @IBAction func cart_btn_action(_ sender: Any) {
        
        
        if let cartcount =  UserDefaults.standard.integer(forKey: "cartCount") as? Int {
            
            if cartcount == 0 {
                
            }else {
                
                let vc = UIStoryboard.init(name: main, bundle: Bundle.main).instantiateViewController(withIdentifier: ViewControllerIdentifiers.CHEERS_CART_VC) as? CartVC
                           self.navigationController?.pushViewController(vc!, animated: true)
            }
            
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }
    
    
    @IBAction func side_menu_btn_action(_ sender: Any) {
        
        self.slideMenuController()?.openLeft()
        
    }
    @IBAction func cat_seeall_btn_action(_ sender: Any) {
        let vc = UIStoryboard.init(name: main, bundle: Bundle.main).instantiateViewController(withIdentifier: ViewControllerIdentifiers.CHEERS_AllCATEGORIES_VC) as? AllCategoriesVC
        isViewAllCategory = true
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func see_all_btn_action(_ sender: UIButton) {
        

        let vc = UIStoryboard.init(name: main, bundle: Bundle.main).instantiateViewController(withIdentifier: ViewControllerIdentifiers.CHEERS_ALLSUBCAT_VC) as? AllSubCategoriesVC
        vc!.sub_cat_title = productDetailModel.productArray[sender.tag].productTitle!
        vc!.sub_catID = productDetailModel.productArray[sender.tag].product_id!
        vc!.isFromHome = true
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productDetailModel.productArray.count
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 270
        
    }
    
    @IBAction func wish_btn_action(_ sender: UIButton) {
        
        let cellInsummaryTag = sender.superview?.superview?.superview?.superview?.superview?.superview?.superview?.superview as? CellProductCategory
        let cellctItem = sender.superview?.superview?.superview as? CellAllCategoryItem
        let tblIndexPath = self.tblview.indexPath(for: cellInsummaryTag!)
        if productDetailModel.productArray[tblIndexPath!.row].sub_product_Array[sender.tag].favourite_status == "0" {
            cellctItem!.wish_btn.setImage( UIImage.init(named: "favrate"), for: .normal)
            productDetailModel.productArray[tblIndexPath!.row].sub_product_Array[sender.tag].favourite_status = "1"
            self.addWishListApi(productID: productDetailModel.productArray[tblIndexPath!.row].sub_product_Array[sender.tag].sub_product_Id!)
            
        }else {
            cellctItem!.wish_btn.setImage( UIImage.init(named: "Unfav"), for: .normal)
            productDetailModel.productArray[tblIndexPath!.row].sub_product_Array[sender.tag].favourite_status = "0"
            self.removeWishListApi(productID: productDetailModel.productArray[tblIndexPath!.row].sub_product_Array[sender.tag].sub_product_Id!)
        }
        self.tblview.reloadRows(at: [tblIndexPath!], with: .none)
        
    }
    
    
    func removeWishListApi(productID : String){
        
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        let parameters: [String: Any] = [
            
            "product_id": productID,
            "user_id": userID,
        ]
            
        Alamofire.request(BASE_URL+"deleteWishList", method: .post, parameters: parameters, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            Utils.hideSpinner()
            if response.result.isSuccess {
                
                let resJson = JSON(response.result.value!)
                if resJson["error"]  == 0 {
                    
                }
                else {
                    
                    
                }
                if response.result.isFailure {
                    let error : Error = response.result.error!
                    Utils.alert(message: error.localizedDescription)
                }
                
            }
        }
    }
    
    
    func searchApi(keyword : String){
        
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        let parameters: [String: Any] = [
            
            "keyword": keyword,
            "user_id": userID,
        ]
        
        
        Alamofire.request(BASE_URL+"search", method: .post, parameters: parameters, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            Utils.hideSpinner()
            if response.result.isSuccess {
                
                let resJson = JSON(response.result.value!)
                if resJson["error"]  == 0 {
                    
                }
                else {
                    
                    
                }
                if response.result.isFailure {
                    let error : Error = response.result.error!
                    Utils.alert(message: error.localizedDescription)
                }
                
            }
        }
    }
    
    
    func addWishListApi(productID : String){
        
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        let parameters: [String: Any] = [
            
            "product_id": productID,
            "user_id": userID,
        ]
                
        Alamofire.request(BASE_URL+"addMyWishList", method: .post, parameters: parameters, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            Utils.hideSpinner()
            if response.result.isSuccess {
                
                let resJson = JSON(response.result.value!)
                if resJson["error"]  == 0 {
                    
                }
                else {
                    
                  //  Utils.alert(message: resJson["message"].string!)
                    
                }
                if response.result.isFailure {
                    let error : Error = response.result.error!
                    Utils.alert(message: error.localizedDescription)
                }
                
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell_category : CellProductCategory = tblview.dequeueReusableCell(withIdentifier: ReusableCellIdentifiers.CHEERS_CELL_PRODUCT_CATEGORY)! as UITableViewCell as! CellProductCategory
        cell_category.lbl_category_name.text = productDetailModel.productArray[indexPath.row].productTitle
        cell_category.clt_product.tag = indexPath.row
        cell_category.btn_seeall_product.tag = indexPath.row
        cell_category.clt_product.reloadData()
        cell_category.selectionStyle = .none
        return cell_category
    }
    
    
    
}


// MARK: -
// MARK: - Collection view methods
extension  HomeVC : UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout
{
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var identifierText = ""
        if collectionView.tag == 100 {
            identifierText = "CellAllcategory"
            let cellCollection = collectionView.dequeueReusableCell(withReuseIdentifier: identifierText, for:
                indexPath as IndexPath) as! CellAllcategory
            let gradientPatternN = colorArray[indexPath.item]
            cellCollection.lbl_category_name?.backgroundColor = UIColor.fromGradient(gradientPatternN.gradient, frame: cellCollection.frame, cornerRadius: 0)
            cellCollection.lbl_category_name.text = productDetailModel.categoriesArray[indexPath.item].cataegoryTitle
            return cellCollection
        }        
        else {
            identifierText = "CellAllCategoryItem"
            let cellCollection = collectionView.dequeueReusableCell(withReuseIdentifier: identifierText, for:
                indexPath as IndexPath) as! CellAllCategoryItem
            cellCollection.cell_bgview.layer.masksToBounds = false
            cellCollection.cell_bgview.layer.shadowColor = UIColor.lightGray.cgColor
            cellCollection.cell_bgview.layer.shadowOffset = CGSize(width: 1, height: 2)
            cellCollection.cell_bgview.layer.shadowRadius = 5
            cellCollection.cell_bgview.layer.shadowOpacity = 0.5
            cellCollection.lbl_qty.text =   productDetailModel.productArray[collectionView.tag].sub_product_Array[indexPath.row].sub_product_price! + " AED"
            cellCollection.wish_btn.tag = indexPath.row
            cellCollection.lbl_product_description.text = productDetailModel.productArray[collectionView.tag].sub_product_Array[indexPath.row].sub_product_name
            
            if productDetailModel.productArray[collectionView.tag].sub_product_Array[indexPath.row].favourite_status == "0" {
                cellCollection.wish_btn.setImage( UIImage.init(named: "favrate"), for: .normal)
                
            }else {
                cellCollection.wish_btn.setImage( UIImage.init(named: "Unfav"), for: .normal)
                
            }
            let imagName = "http://cheersonlinesales.com/uploads/" + "\(productDetailModel.productArray[collectionView.tag].sub_product_Array[indexPath.row].sub_product_image!)"
            cellCollection.image_view?.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cellCollection.image_view?.sd_setImage(with:URL(string: imagName) , placeholderImage: nil, options: .highPriority, completed: { (imageNew, error, cache, url) in
                if(error != nil)
                {
                    cellCollection.image_view.contentMode =  .scaleAspectFit
                    cellCollection.image_view?.image = UIImage(named:"bg_img")
                }
                else
                {
                    cellCollection.image_view?.image = imageNew
                }
                
            })
            
            return cellCollection
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView.tag == 100 {
            
            return self.productDetailModel.categoriesArray.count
        }
        else {
            
            return productDetailModel.productArray[collectionView.tag].sub_product_Array.count
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView.tag == 100 {
            let size: CGSize = productDetailModel.categoriesArray[indexPath.item].cataegoryTitle!.size(withAttributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15.0)])
            return CGSize(width: size.width+45 , height: 50)
        }else {
            return CGSize(width: collectionView.frame.size.width/2, height: 230)
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        if collectionView.tag == 100 {
            
            let vc = UIStoryboard.init(name: main, bundle: Bundle.main).instantiateViewController(withIdentifier: ViewControllerIdentifiers.CHEERS_AllCATEGORIES_VC) as? AllCategoriesVC
            isViewAllCategory = false
            vc!.cat_title = productDetailModel.categoriesArray[indexPath.row].cataegoryTitle!
            vc!.catID = productDetailModel.categoriesArray[indexPath.row].id!
            self.navigationController?.pushViewController(vc!, animated: true)

          
            
        }else {
            
            let vc = UIStoryboard.init(name: main, bundle: Bundle.main).instantiateViewController(withIdentifier: ViewControllerIdentifiers.CHEERS_ITEMDESCRIPTION_VC) as? ItemDescription
                      vc!.item_description_array = productDetailModel.productArray[collectionView.tag].sub_product_Array[indexPath.row]
                      self.navigationController?.pushViewController(vc!, animated: true)
        }
    }
}


extension UISearchBar {
    
    // Due to searchTextField property who available iOS 13 only, extend this property for iOS 13 previous version compatibility
    var compatibleSearchTextField: UITextField {
        guard #available(iOS 13.0, *) else { return legacySearchField }
        return self.searchTextField
    }
    
    private var legacySearchField: UITextField {
        if let textField = self.subviews.first?.subviews.last as? UITextField {
            
            textField.font =  UIFont.systemFont(ofSize: 11)
            
            // Xcode 11 previous environment
            return textField
        } else if let textField = self.value(forKey: "searchField") as? UITextField {
            
            textField.font =  UIFont.systemFont(ofSize: 11)
            
            return textField
        } else {
            // exception condition or error handler in here
            return UITextField()
        }
    }
}


extension UICollectionView {
    func indexPathForView(view: AnyObject) -> IndexPath? {
        let originInCollectioView = self.convert(CGPoint.zero, from: (view as! UIView))
        return self.indexPathForItem(at: originInCollectioView) as IndexPath?
    }
}
