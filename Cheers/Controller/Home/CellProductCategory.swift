//
//  CellProductCategory.swift
//  Cheers
//
//  Created by Dilip Mishra on 30/04/20.
//  Copyright © 2020 Dilip Mishra. All rights reserved.
//

import UIKit

class CellProductCategory: UITableViewCell {
    
    
    @IBOutlet weak var btn_seeall_product: UIButton!
    @IBOutlet weak var lbl_category_name: UILabel!
    @IBOutlet weak var clt_product: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
