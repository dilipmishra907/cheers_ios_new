//
//  ItemDescription.swift
//  Cheers
//
//  Created by Dilip Mishra on 26/04/20.
//  Copyright © 2020 Dilip Mishra. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class ItemDescription: UIViewController  {
    @IBOutlet weak var cltPagDetails: UICollectionView!
    @IBOutlet weak var item_price: UILabel!
    @IBOutlet weak var item_discount_price: UILabel!
    @IBOutlet weak var lbl_item_description: UILabel!
    @IBOutlet weak var lbl_product_name: UILabel!
    @IBOutlet weak var img_product_details: UIImageView!
    @IBOutlet weak var view_product_details: UIView!
    var item_description_array = HomeSubProductModel()
    var selectedItemStr = ""
    var selectedItemPrice = ""
    var selectedItemPagID = ""

    @IBOutlet weak var lbl_product_code: UILabel!
    @IBOutlet weak var lbl_country: UILabel!
    @IBOutlet weak var lbl_brand: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        selectedItemPagID = "0"
        lbl_product_code.text = "Product Code: " +  item_description_array.sub_product_product_code!
        lbl_country.text = "Country: " +  item_description_array.sub_product_country!
        lbl_brand.text = "Brand: " +  item_description_array.sub_product_brand!
        self.view.backgroundColor = UIColor.white
        item_price.attributedText = " 62 AED ".strikeThrough()
        item_price.isHidden = true
        view_product_details.layer.masksToBounds = false
        view_product_details.layer.shadowColor = UIColor.lightGray.cgColor
        view_product_details.layer.shadowOffset = CGSize(width: 1, height: 2)
        view_product_details.layer.shadowRadius = 5
        view_product_details.layer.shadowOpacity = 0.5
        lbl_product_name.text = item_description_array.sub_product_name
        lbl_item_description.text = item_description_array.sub_product_descriptionn
        lbl_product_name.text = item_description_array.sub_product_name
        item_price.text = item_description_array.sub_product_price! + " AED"
        
        if item_description_array.pack_array.count == 0 {
            item_discount_price.text =  item_description_array.sub_product_offer_price! + " AED"

        }else {
            
            for i in (0..<item_description_array.pack_array.count) {
                if  item_description_array.pack_array[i].pack_name == "EA" {
                    selectedItemPrice = item_description_array.pack_array[i].pack_price!
                    item_discount_price.text = selectedItemPrice + " AED"
                }
            }
        }
        
        
        
        let imagName = "http://cheersonlinesales.com/uploads/" + "\(item_description_array.sub_product_image!)"
        img_product_details.sd_setImage(with: URL(string:imagName), placeholderImage: nil)
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func plus_btn_action(_ sender: UIButton) {
        
        
    }
    @IBAction func minus_btn_action(_ sender: UIButton) {
        
        
    }
    
    @IBAction func add_tocart_btn_action(_ sender: Any) {
        
        
        if selectedItemPagID == "" {
            Utils.alert(message: "Please select Pack")

        }else {
            self.addToCartApi(productID: item_description_array.sub_product_product_id!)

        }
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func addToCartApi(productID : String){
        
        Utils.showSpinner()
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        let parameters: [String: Any] = [
            "product_id": productID,
            "flag": "plus",
            "product_quantity": "1",
            "user_id": userID,
            "pack_id": selectedItemPagID,
        ]
    

        
        Alamofire.request(BASE_URL+"addToCart", method: .post, parameters: parameters, encoding:  URLEncoding.httpBody, headers: headers).responseJSON { (response:DataResponse<Any>) in
            Utils.hideSpinner()
            
            
            switch response.result{
            case .success(let json):
                let resJson = JSON(response.result.value!)
                
        
                
                if resJson["error"]  == false {
                        
                    
                    let vc = UIStoryboard.init(name: main, bundle: Bundle.main).instantiateViewController(withIdentifier: ViewControllerIdentifiers.CHEERS_CART_VC) as? CartVC
                           self.navigationController?.pushViewController(vc!, animated: true)
                        
                }
                else {
                    Utils.alert(message: resJson["message"].string!)
                }
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    
    
}


// MARK: -
// MARK: - Collection view methods
extension  ItemDescription : UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout
{
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var identifierText = ""
        identifierText = "CellQty"
        let cellCollection = collectionView.dequeueReusableCell(withReuseIdentifier: identifierText, for:
            indexPath as IndexPath) as! CellItemQty
        cellCollection.lbl_qty.text = item_description_array.pack_array[indexPath.row].pack_name
        
        
        
        if selectedItemStr == "" && item_description_array.pack_array[indexPath.row].pack_name == "EA" {
            cellCollection.lbl_qty.layer.borderColor = UIColor.systemBlue.cgColor
            cellCollection.lbl_qty.backgroundColor = UIColor.systemBlue
            cellCollection.lbl_qty.textColor = UIColor.white
            selectedItemPagID = item_description_array.pack_array[indexPath.item].pack_ID!

            }
        
       else if selectedItemStr == item_description_array.pack_array[indexPath.row].pack_name {
            cellCollection.lbl_qty.layer.borderColor = UIColor.systemBlue.cgColor
            cellCollection.lbl_qty.backgroundColor = UIColor.systemBlue
            cellCollection.lbl_qty.textColor = UIColor.white


        }else {
            cellCollection.lbl_qty.layer.borderColor = UIColor.darkGray.cgColor
            cellCollection.lbl_qty.backgroundColor = UIColor.white
            cellCollection.lbl_qty.textColor = UIColor.darkGray
           // selectedItemPagID = item_description_array.pack_array[indexPath.row].pack_ID!


            }
        
        return cellCollection
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return item_description_array.pack_array.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                           layout collectionViewLayout: UICollectionViewLayout,
                           sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size: CGSize = item_description_array.pack_array[indexPath.row].pack_name!.size(withAttributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16.0)])
                        return CGSize(width: size.width+45 , height: 50)
            }
    

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
        selectedItemStr = item_description_array.pack_array[indexPath.row].pack_name!
        selectedItemPrice = item_description_array.pack_array[indexPath.row].pack_price!
        item_discount_price.text = selectedItemPrice + " AED"
        selectedItemPagID = item_description_array.pack_array[indexPath.row].pack_ID!
        cltPagDetails.reloadData()
    }
}


extension String {
    func strikeThrough() -> NSAttributedString {
        let attributeString =  NSMutableAttributedString(string: self)
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: NSUnderlineStyle.single.rawValue, range: NSMakeRange(0,attributeString.length))
        return attributeString
    }
}

