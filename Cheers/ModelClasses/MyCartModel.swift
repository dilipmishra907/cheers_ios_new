//
//  MyCartModel.swift
//  Cheers
//
//  Created by Dilip Mishra on 12/05/20.
//  Copyright © 2020 Dilip Mishra. All rights reserved.
//

import UIKit

class MyCartModel: NSObject {
    
    var cart_Id :String?
    var cart_product_Id :String?
    var cart_product_productTitle :String?
    var cart_product_code:String?
    var cart_product_brand :String?
    var cart_product_country:String?
    var cart_product_name :String?
    var cart_product_image:String?
    var cart_product_category_id:String?
    var cart_product_price : String?
    var cart_product_quantity:String?
    var cart_product_offer_price:String?
    var cart_product_tax_price:String?
    var cart_product_offer_percentage:String?
    var cart_product_measure:String?
    var cart_product_descriptionn:String?
    var cart_cateID:String?
    var brand:String?
    var pack_id:String?
    var cart_product_product_id:String?
    var cart_product_stock_quantity:String?
    var user_id:String?
    var offer_status : Bool?
    var favourite_status : Bool?
    var cart_packPrice:String?

}
