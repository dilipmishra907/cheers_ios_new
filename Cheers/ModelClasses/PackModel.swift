//
//  PackModel.swift
//  Cheers
//
//  Created by Dilip Mishra on 26/05/20.
//  Copyright © 2020 Dilip Mishra. All rights reserved.
//

import UIKit

class PackModel: NSObject {
    var pack_name :String?
    var pack_ID : String?
    var pack_size :String?
    var pack_price : String?
    var pack_productid : String?
    
}
