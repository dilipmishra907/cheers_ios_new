//
//  utilityParsing.swift
//  MILaundry
//
//  Created by Dilip Mishra on 25/12/19.
//  Copyright © 2019 Dilip Mishra. All rights reserved.
//

import Foundation

class parser
{
    static func JsonToHomeModal(dict: [String:Any])-> HomeModel {
        
        let productModelData:HomeModel = HomeModel()
        if let requestdCatArray =  dict["Categories"] as? [[String : Any]]
        {
            productModelData.categoriesArray = JsonToCategoryModal(Array: requestdCatArray)
        }
        else
        {
            productModelData.categoriesArray = []
        }
        
        if let requestdProductArray =  dict["Products"] as? [[String : Any]]
        {
            productModelData.productArray = JsonToProductModal(dict: requestdProductArray)
        }
        else
        {
            productModelData.productArray = []
        }
        return productModelData
        
    }
    
    static func JsonToCategoryModal(Array: [[String:Any]])->[CategoriesModel] {
        
        var catHomeArray:[CategoriesModel]=[CategoriesModel]()
        for dataValue in Array
        {
            let catModelObject : CategoriesModel = CategoriesModel()
            if let id = dataValue["id"] as? String
            {
                catModelObject.id = id
            }
            else
            {
                catModelObject.id = "0"
            }
            
            if let str_cat = dataValue["category"] as? String
            {
                catModelObject.cataegoryTitle = str_cat
            }
            else
            {
                catModelObject.cataegoryTitle = ""
            }
            catHomeArray.append(catModelObject)
        }
        return catHomeArray
        
    }
    
    
    static func JsonToProductModal(dict: [[String:Any]])->[ProductModel] {
        var productHomeArray:[ProductModel]=[ProductModel]()
        for dataValue in dict
        {
            let productHomeObject : ProductModel = ProductModel()
            if let strTitle = dataValue["title"] as? String
            {
                productHomeObject.productTitle = strTitle
            }
            else
            {
                productHomeObject.productTitle = ""
            }
            if let productId = dataValue["id"] as? String
            {
                productHomeObject.product_id = productId
            }
            else
            {
                productHomeObject.product_id = "0"
            }
            if let productArray =  dataValue["products"] as? [[String : Any]]
            {
                productHomeObject.sub_product_Array = JsonToHomeSubProductModal(Array: productArray)
            }
            else
            {
                productHomeObject.sub_product_Array = []
            }
            productHomeArray.append(productHomeObject)
        }
        return productHomeArray
    }
    
    static func JsonToHomeSubProductModal(Array: [[String:Any]])->[HomeSubProductModel] {
        var subproductHomeArray:[HomeSubProductModel]=[HomeSubProductModel]()
        for dataValue in Array
        {
            let subproductHomeObject : HomeSubProductModel = HomeSubProductModel()
            if let strId = dataValue["id"] as? String
            {
                subproductHomeObject.sub_product_Id = strId
            }
            else
            {
                subproductHomeObject.sub_product_Id = "0"
            }
            
            
            if let packtArray =  dataValue["packs"] as? [[String : Any]]
            {
                subproductHomeObject.pack_array = JsonToPackModel(Array: packtArray)
            }
            else
            {
                subproductHomeObject.pack_array = []
            }
            
            if let fav = dataValue["favourite_status"] as? String
                       {
                           subproductHomeObject.favourite_status = fav
                       }
                       else
                       {
                           subproductHomeObject.favourite_status = "0"
                       }
            
            if let str_productcode = dataValue["product_code"] as? String
            {
                subproductHomeObject.sub_product_product_code = str_productcode
            }
            else
            {
                subproductHomeObject.sub_product_product_code = ""
            }
            if let str_brand = dataValue["brand"] as? String
            {
                subproductHomeObject.sub_product_brand = str_brand
            }
            else
            {
                subproductHomeObject.sub_product_brand = ""
            }
            
            if let str_brand = dataValue["country"] as? String
            {
                subproductHomeObject.sub_product_country = str_brand
            }
            else
            {
                subproductHomeObject.sub_product_country = ""
            }
            
            if let str_name = dataValue["name"] as? String
            {
                subproductHomeObject.sub_product_name = str_name
            }
            else
            {
                subproductHomeObject.sub_product_name = ""
            }
            
            if let str_image = dataValue["image"] as? String
            {
                subproductHomeObject.sub_product_image = str_image
            }
            else
            {
                subproductHomeObject.sub_product_image = ""
            }
            if let str_image = dataValue["price"] as? String
            {
                subproductHomeObject.sub_product_price = str_image
            }
            else
            {
                subproductHomeObject.sub_product_price = ""
            }
            if let str_quantity = dataValue["quantity"] as? String
            {
                subproductHomeObject.sub_product_quantity = str_quantity
            }
            else
            {
                subproductHomeObject.sub_product_quantity = ""
            }
            
            if let str_offerprice = dataValue["offer_price"] as? String
            {
                subproductHomeObject.sub_product_offer_price = str_offerprice
            }
            else
            {
                subproductHomeObject.sub_product_offer_price = "0"
            }
            
            if let str_descr = dataValue["description"] as? String
            {
                subproductHomeObject.sub_product_descriptionn = str_descr
            }
            else
            {
                subproductHomeObject.sub_product_descriptionn = ""
            }
            
            if let str_measure = dataValue["measure"] as? String
            {
                subproductHomeObject.sub_product_measure = str_measure
            }
            else
            {
                subproductHomeObject.sub_product_measure = ""
            }
            
            
            
            if let str_productID = dataValue["product_id"] as? String
            {
                subproductHomeObject.sub_product_product_id = str_productID
            }
            else
            {
                subproductHomeObject.sub_product_product_id = "0"
            }
            
            
            
            if let str_user_id = dataValue["user_id"] as? String
            {
                subproductHomeObject.user_id = str_user_id
            }
            else
            {
                subproductHomeObject.user_id = "0"
            }
            subproductHomeArray.append(subproductHomeObject)
        }
        return subproductHomeArray
    }
    
    
    static func JsonToMyCartModal(Array: [[String:Any]])->[MyCartModel] {
        var subproductHomeArray:[MyCartModel]=[MyCartModel]()
        for dataValue in Array
        {
            let subproductHomeObject : MyCartModel = MyCartModel()
            if let strId = dataValue["product_id"] as? String
            {
                subproductHomeObject.cart_product_Id = strId
            }
            else
            {
                subproductHomeObject.cart_product_Id = "0"
            }
            
            if let strId = dataValue["id"] as? String
            {
                subproductHomeObject.cart_Id = strId
            }
            else
            {
                subproductHomeObject.cart_Id = "0"
            }
            
            if let strId = dataValue["pack_id"] as? String
            {
                subproductHomeObject.pack_id = strId
            }
            else
            {
                subproductHomeObject.pack_id = "0"
            }
            
            if let strBrand = dataValue["brand"] as? String
            {
                subproductHomeObject.brand = strBrand
            }
            else
            {
                subproductHomeObject.brand = "brand"
            }
            
            if let str_quantity = dataValue["cart_product_quantity"] as? String
            {
                subproductHomeObject.cart_product_quantity = str_quantity
            }
            else
            {
                subproductHomeObject.cart_product_quantity = ""
            }
            
            
            if let str_productcode = dataValue["product_code"] as? String
            {
                subproductHomeObject.cart_product_code = str_productcode
            }
            else
            {
                subproductHomeObject.cart_product_code = ""
            }
            if let str_brand = dataValue["brand"] as? String
            {
                subproductHomeObject.cart_product_brand = str_brand
            }
            else
            {
                subproductHomeObject.cart_product_brand = ""
            }
            
            if let str_brand = dataValue["country"] as? String
            {
                subproductHomeObject.cart_product_country = str_brand
            }
            else
            {
                subproductHomeObject.cart_product_country = ""
            }
            
            if let str_name = dataValue["name"] as? String
            {
                subproductHomeObject.cart_product_name = str_name
            }
            else
            {
                subproductHomeObject.cart_product_name = ""
            }
            
            if let str_image = dataValue["image"] as? String
            {
                subproductHomeObject.cart_product_image = str_image
            }
            else
            {
                subproductHomeObject.cart_product_image = ""
            }
            if let str_image = dataValue["price"] as? String
            {
                subproductHomeObject.cart_product_price = str_image
            }
            else
            {
                subproductHomeObject.cart_product_price = ""
            }
            
            
            if let str_offerprice = dataValue["offer_price"] as? String
            {
                subproductHomeObject.cart_product_offer_price = str_offerprice
            }
            else
            {
                subproductHomeObject.cart_product_offer_price = "0"
            }
            
            if let str_descr = dataValue["description"] as? String
            {
                subproductHomeObject.cart_product_descriptionn = str_descr
            }
            else
            {
                subproductHomeObject.cart_product_descriptionn = ""
            }
            
            if let str_user_id = dataValue["user_id"] as? String
            {
                subproductHomeObject.user_id = str_user_id
            }
            else
            {
                subproductHomeObject.user_id = "0"
            }
            if let str_pack_price = dataValue["pack_price"] as? String
                       {
                           subproductHomeObject.cart_packPrice = str_pack_price
                       }
                       else
                       {
                           subproductHomeObject.cart_packPrice = "0"
                       }
            if let str_catID = dataValue["category_id"] as? String
            {
                subproductHomeObject.cart_cateID = str_catID
            }
            else
            {
                subproductHomeObject.cart_cateID = "0"
            }
            subproductHomeArray.append(subproductHomeObject)
        }
        return subproductHomeArray
    }
    
    static func JsonToPackModel(Array: [[String:Any]])->[PackModel] {
        
        var allPackDeatilArray:[PackModel]=[PackModel]()
        for dataValue in Array
        {
            let packModelObject : PackModel = PackModel()
            if let pack_name = dataValue["pack_name"] as? String
            {
                packModelObject.pack_name = pack_name
            }
            else
            {
                packModelObject.pack_name = "0"
            }
            
            if let str_pack_id = dataValue["pack_id"] as? String
            {
                packModelObject.pack_ID = str_pack_id
            }
            else
            {
                packModelObject.pack_ID = ""
            }
            
            if let str_pack_size = dataValue["pack_size"] as? String
            {
                packModelObject.pack_size = str_pack_size
            }
            else
            {
                packModelObject.pack_size = ""
            }
            
            if let str_pack_price = dataValue["pack_price"] as? String
            {
                packModelObject.pack_price = str_pack_price
            }
            else
            {
                packModelObject.pack_price = "0"
            }
            
            if let str_product_ID = dataValue["pack_productid"] as? String
                      {
                          packModelObject.pack_productid = str_product_ID
                      }
                      else
                      {
                          packModelObject.pack_productid = "0"
                      }
            
            allPackDeatilArray.append(packModelObject)
        }
        return allPackDeatilArray
    }
    
    
    static func JsonToAllSubCategoryModal(Array: [[String:Any]])->[AllSubCategoryModel] {
        
        var allCatArray:[AllSubCategoryModel]=[AllSubCategoryModel]()
        for dataValue in Array
        {
            let catModelObject : AllSubCategoryModel = AllSubCategoryModel()
            if let id = dataValue["id"] as? String
            {
                catModelObject.allcat_id = id
            }
            else
            {
                catModelObject.allcat_id = "0"
            }
            
            if let str_cat = dataValue["category"] as? String
            {
                catModelObject.all_cataegoryTitle = str_cat
            }
            else
            {
                catModelObject.all_cataegoryTitle = ""
            }
            
            if let str_img = dataValue["image"] as? String
            {
                catModelObject.all_image = str_img
            }
            else
            {
                catModelObject.all_image = ""
            }
            
            if let parant_cat = dataValue["parent_category"] as? String
            {
                catModelObject.all_parant_category = parant_cat
            }
            else
            {
                catModelObject.all_parant_category = "0"
            }
            
            allCatArray.append(catModelObject)
        }
        return allCatArray
    }
}



