//
//  HomeSubProductModel.swift
//  Cheers
//
//  Created by Dilip Mishra on 07/05/20.
//  Copyright © 2020 Dilip Mishra. All rights reserved.
//

import UIKit

class HomeSubProductModel: NSObject {
    
    var pack_array = [PackModel]()

    var sub_product_Id :String?
    var sub_product_productTitle :String?
    var sub_product_product_code:String?
    var sub_product_brand :String?
    var sub_product_country:String?
    var sub_product_name :String?
    var sub_product_image:String?
    var sub_product_category_id:String?
    var sub_product_price : String?
    var sub_product_quantity:String?
    var sub_product_offer_price:String?
    var sub_product_tax_price:String?
    var sub_product_offer_percentage:String?
    var sub_product_measure:String?
    var sub_product_descriptionn:String?
    var sub_product_product_id:String?
    var sub_product_stock_quantity:String?
    var user_id:String?
    var offer_status : Bool?
    var favourite_status : String?

    
    


}
