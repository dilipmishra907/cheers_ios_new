//
//  ProductDetailModel.swift
//  Cheers
//
//  Created by Dilip Mishra on 07/05/20.
//  Copyright © 2020 Dilip Mishra. All rights reserved.
//

import UIKit

class HomeModel: NSObject  {
    
    var categoriesArray = [CategoriesModel]()
    var productArray = [ProductModel]()
    var cloth_code : String?
    var cloth_image :String?
    var cloth_type : String?
    var cloth_serviceType : String?
    var cloth_serviceID : String?
    var cloth_type_lang :String?
    var garment_category_id:String?
    var id:String?
    var price:String?
    var qty : Int?
    var isPlus : Bool?

}

