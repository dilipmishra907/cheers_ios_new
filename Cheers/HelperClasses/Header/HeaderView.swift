////
////  HeaderView.swift
////  SGDoc
////
////  Created by Spicejet LTD on 24/01/20.
////  Copyright © 2020 Spicejet LTD. All rights reserved.
////
//
//import UIKit
//
//class HeaderView: UIView ,UIPopoverPresentationControllerDelegate{
//
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//
//        guard let view = loadViewFromNib() else { return }
//        view.frame = self.bounds
//        self.addSubview(view)
//
//
//    }
//   
//    @IBAction func settingBtnAction(_ sender: UIButton) {
//
//        let popoverContent = UIStoryboard.init(name: main, bundle: Bundle.main).instantiateViewController(withIdentifier: ViewControllerIdentifiers.ETECHLOG_POPOVER_VIEWCONTROLLER) as? PopOverVC
//         let nav = UINavigationController(rootViewController: popoverContent!)
//         nav.modalPresentationStyle = UIModalPresentationStyle.popover
//         nav.modalPresentationStyle = UIModalPresentationStyle.popover
//         nav.navigationBar.isHidden = true
//         nav.modalTransitionStyle = .crossDissolve
//
//         let popover = nav.popoverPresentationController
//         let size = CGSize(width: 230, height:40)
//         popoverContent!.preferredContentSize = size
//         popover?.delegate = self
//         popover?.sourceView = sender
//         popover?.permittedArrowDirections = .up
//         popover?.sourceRect = CGRect(origin: CGPoint(x:20,y :40), size: CGSize(width: 0, height: 0))
//         let parentViewController: UIViewController = UIApplication.shared.windows[1].rootViewController!
//         popoverContent!.modalPresentationStyle = .overCurrentContext
//         parentViewController.present(nav, animated: true, completion: nil)
//    }
//
//    func loadViewFromNib() -> UIView? {
//        let bundle = Bundle(for: type(of: self))
//        let nib = UINib(nibName: ReusableCellIdentifiers.HEADER_VIEW, bundle: bundle)
//        return nib.instantiate(withOwner: self, options: nil).first as? UIView
//    }
//
//    @IBAction func backBtnAction(_ sender: UIButton) {
//
//        let controller_name:[String: Any] = ["controlleraName": "abc"]
//               NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NotificationIdentifier"), object: nil, userInfo: controller_name)
//    }
//
//
//
//    // MARK: - Popover delegate methods
//       func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
//           return .none
//       }
//
//       func  popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) {
//           print("dismissed")
//       }
//}
//
