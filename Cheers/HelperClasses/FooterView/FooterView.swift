//
//  FooterView.swift
//  EtechLog
//
//  Created by Dilip Mishra on 13/04/20.
//  Copyright © 2020 Dilip Mishra. All rights reserved.
//

import UIKit

class FooterView: UIView {

    required init?(coder aDecoder: NSCoder) {
           super.init(coder: aDecoder)
           guard let view = loadViewFromNib() else { return }
           view.frame = self.bounds
           self.addSubview(view)
       }
    
    func loadViewFromNib() -> UIView? {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: ReusableCellIdentifiers.FOOTER_VIEW, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as? UIView
    }
}
