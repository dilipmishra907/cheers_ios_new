//
//  Constant.swift
//  SGDoc
//  Created by Spicejet LTD on 15/01/20.
//  Copyright © 2020 Spicejet LTD. All rights reserved.
//

import Foundation
import UIKit

let main = "Main"
var isViewAllCategory = false
var strCurrentDate = ""
var isChnageAddress = false
let BASE_URL = "http://cheersonlinesales.com/api/"
var all_categoriesArray = [CategoriesModel]()
var userDetails = [String: Any]()
var defaultAddressDic = [String: Any]()

var userID = ""

var isFromCheckout = false


var user_latitude  :  Double? = 0
var user_longitude :  Double? = 0


var addressID = ""
var img_first_name = ""
var image_second_name  = ""
var str_address  = ""
var str_address1  = ""





struct GlobalConstants {

        static let kColor_Seperator: UIColor = UIColor(red: 53.0/255.0, green: 126.0/255.0, blue: 167.0/255.0, alpha: 1.0)
        static let kColor_orange: UIColor = UIColor(red: 255.0/255.0, green: 147.0/255.0, blue: 38.0/255.0, alpha: 1.0)
        static let kColor_NonCompliant: UIColor = UIColor(red: 190.0/255.0, green: 15.0/255.0, blue: 52.0/255.0, alpha: 1.0)
}



struct ReusableCellIdentifiers {
    static let CHEERS_CELL_PRODUCT_CATEGORY = "CellProductCategory"
    static let MENU_CELL = "MenuCell"
    static let FOOTER_VIEW = "FooterView"
    static let HEADER_VIEW = "HeaderView"
    static let SYNC_CELL = "CellSyncData"
    static let ELIBRARY_CELL = "CellSyncData"
    static let ELIBRARY_MAIN_CELL = "CellSyncDataMain"
    static let ELIBRARY_PARENTS_CELL = "CellSyncDataParant"
    static let ELIBRARY_CHILD_CELL = "CellSyncDataChild"
    static let CUSTOMPOPUP_VIEW = "CustomPopup"
    static let REVISIONHIGHLIGHT_CELL = "CellRevisionHighLight"
    
}

struct ViewControllerIdentifiers {
    static let CHEERS_LOGIN_VC = "ViewController"
    static let CHEERS_REGISTER_VC = "RegisterVc"
    static let CHEERS_PROFILE_VC = "ProfileVC"
    static let CHEERS_AllCATEGORIES_VC = "AllCategoriesVC"
    static let CHEERS_ORDERDETAILS_VC = "OrderDetails"
    static let CHEERS_CHECKOUT_VC = "CheckoutVC"
    static let CHEERS_ITEMDESCRIPTION_VC = "ItemDescription"
    static let CHEERS_ALLSUBCAT_VC = "AllSubCategoriesVC"
    static let CHEERS_HOME_VC = "HomeVC"
    static let CHEERS_UPLOADOCUMENT_VC = "UploadDocumentVC"
    static let CHEERS_CONGRULATIONS_VC = "CongrulationVC"
    static let CHEERS_FORGOTPASSWORD_VC = "ForgotPasswordVC"
    static let CHEERS_CART_VC = "CartVC"
    static let CHEERS_ADDADDRESS_VC = "AddAddressVC"
    static let CHEERS_SETTINGS_VC = "SettingsVC"
    static let CHEERS_SEARCH_VC = "SearchViewController"
    static let CHEERS_PAYUPON_VC = "PayUponDeliveryVC"



}


struct AlertConstants {
    static let INVALID_PASSWORD = "Invalid Password"
    static let COMING_SOON = "Coming Soon"
    static let LOGOUT_TITLE = "Log Out"
    static let LOGOUT_MESSSAGE = "Are you sure that you want to log out?"
    static let INVALID_USERNAME = "Invalid User name"
    static let NO_DATA_AVAILABLE = "No data available"
    static let USER_NOT_FOUND = "Invalid Credentails"
    static let BOOKMARK_CONFIRMATION = "Are you sure you want to bookmark?"
    static let REMOVE_BOOKMARK_CONFIRMATION = "Are you sure you want to remove the bookmark?"
    static let WENT_WRONG = "You are offline"
    static let OFFLINE_GO_TO_BOOKMARK = "You are offline and you have not saved any document(s)"
    static let OFFLINE_SEE_SAVED_BOOKMARK = "You are offline either come online or go to bookmark to see saved document(s)"
    static let CANCEL_DOWNLOAD = "Cancel Sync Documents"
    static let CANCEL_DOWNLOAD_MESSAGE = "Are you sure to cancel documents sync?"
    static let WEAK_CONNECTION = "Please check your internet connection, seems too low to download"
    static var HOME_TITLE  = "Home"
    static var SETTING_TITLE = "Settings"
    static var ELibray_TITLE = "E-Library"
    static var SYNC_TITLE = "Sync"
    static let YES = "Yes"
    static let NO = "No"
    static let OK = "Ok"
}

struct MenuOptions {
    static let DOC_REPO = "Document Repository"
    static let BOOKMARK = "Bookmark"
    static let LOGOUT = "Sync"
}


