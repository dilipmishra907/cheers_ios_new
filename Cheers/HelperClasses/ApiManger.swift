//
//  ApiManger.swift
//  RGB
//
//  Created by Apple on 14/10/18.
//  Copyright © 2018 sidhdharth joshi. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ApiManger: NSObject {
    
    static let sharedInstance = ApiManger()

    func requestGETURL(_ strURL: String, success:@escaping (Any?) -> Void, failure:@escaping (Error) -> Void)
    {
        Utils.showSpinner()

        Alamofire.request(strURL).responseJSON { (responseObject) -> Void in
            Utils.hideSpinner()

            if responseObject.result.isSuccess {
                success((responseObject.result.value as! String).convertToDictionary())
            }
            
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                failure(error)
            }
        }
    }
    
    func requestPOSTURL(_ strURL : String, params : [String : AnyObject]?, headers : [String : String]?, success:@escaping (Any?) -> Void, failure:@escaping (Error) -> Void){
        Utils.showSpinner()
        Alamofire.request(strURL, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (responseObject) -> Void in
            Utils.hideSpinner()
            if responseObject.result.isSuccess {
                
                let resJson = JSON(responseObject.result.value!)
                success(resJson)

             
            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                failure(error)
            }
        }
    }
}



