//  Utils.swift
//  SGDoc
//
//  Created by Spicejet LTD on 15/01/20.
//  Copyright © 2020 Spicejet LTD. All rights reserved.
//

import UIKit

extension UIColor {
    @nonobjc class var appGrayColor: UIColor {
        return UIColor(red: 103.0 / 255.0, green: 103.0 / 255.0, blue: 103.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var appRedColor: UIColor {
        return UIColor.red
       }
    @nonobjc class var appOrangeColor: UIColor {
           return UIColor.systemOrange
          }
    @nonobjc class var appClearColor: UIColor {
     return UIColor.clear
    }
    @nonobjc class var appBlackColor: UIColor {
        return UIColor(red: 5.0/255.0, green: 18.0/255.0, blue: 32.0/255.0, alpha: 1.0)
    }
    @nonobjc class var appBlueColor: UIColor {
        return UIColor(red: 16.0/255.0, green: 75.0/255.0, blue: 169.0/255.0, alpha: 1.0)
    }
    @nonobjc class var appGreenBoarderColor: UIColor {
        return UIColor(red: 73.0/255.0, green: 137.0/255.0, blue: 140.0/255.0, alpha: 1.0)
    }
    @nonobjc class var appNightModeTabColor: UIColor {
           return UIColor(red: 54.0/255.0, green: 54.0/255.0, blue: 60.0/255.0, alpha: 1.0)
       }
    @nonobjc class var appNormaalTabColor: UIColor {
        return UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    }
    @nonobjc class var appNightModeBgColor
        : UIColor {
              return UIColor(red: 32.0/255.0, green: 31.0/255.0, blue: 36.0/255.0, alpha: 1.0)
          }
    
    @nonobjc class var appNightModeTabbarTextColor: UIColor {
              return UIColor(red: 195.0/255.0, green: 195.0/255.0, blue: 195.0/255.0, alpha: 1.0)
          }
}
