//
//  HelperMethodsClass.swift
//  SGDoc
//
//  Created by Spicejet LTD on 11/01/20.
//  Copyright © 2020 Spicejet LTD. All rights reserved.
//

import Foundation
import UIKit

class HelperMethodsClass {
    
    static let shared = HelperMethodsClass()

    private init() {
    }
    
    func isValidPassword(password:String?) -> Bool {
        guard password != nil else { return false }
        
        // at least one uppercase,
        // at least one digit
        // at least one lowercase
        // 8 characters total
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).{8,}")
        return passwordTest.evaluate(with: password)
    }
    
    func isValidEmail(email:String?) -> Bool {
        guard email != nil else { return false }
        
        let regEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let pred = NSPredicate(format:"SELF MATCHES %@", regEx)
        return pred.evaluate(with: email)
    }
    
    func showAlert(withTitle title: String, andMessage message: String, inViewController viewController: UIViewController) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: AlertConstants.OK, style: .default) { (action:UIAlertAction) in
        }
        
        alertController.addAction(okAction)
        
        viewController.present(alertController, animated: true, completion: nil)
    }
    
    func getDatefromTimeStamp (str_date : String) -> String
    {
        var dateStr : String = ""
        
        if let index = str_date.range(of: "T")?.lowerBound {
            let substring = str_date[..<index]
            dateStr = String(substring)
        }
        return dateStr
    }
    
    func getCurrentDate() -> String {
        let date = Date()
        let format = DateFormatter()
       // format.dateFormat = "YYYY/MM/dd HH:mm'"
        format.dateFormat = "dd/MM/YYYY HH:mm'"
        let formattedDate = format.string(from: date)
        return formattedDate
    }
    
    func fullStringToDateConversion(inputString: String) -> Date{
        let df = DateFormatter()
        df.dateFormat = "YYYY-MM-dd'T'HH:mm:ss'"
        let newDate:Date = df.date(from: inputString)!
        print(newDate)
        return newDate
    }
}


extension UIView{
    
    func activityStartAnimating(activityColor: UIColor, backgroundColor: UIColor) {
        let backgroundView = UIView()
        backgroundView.frame = CGRect.init(x: 0, y: 0, width: self.bounds.width, height: self.bounds.height)
        backgroundView.backgroundColor = backgroundColor
        backgroundView.tag = 475647
        
        var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
        activityIndicator = UIActivityIndicatorView(frame: CGRect.init(x: 0, y: 0, width: 100, height: 100))
        activityIndicator.center = self.center
        activityIndicator.transform = CGAffineTransform(scaleX: 3, y: 3)
        activityIndicator.hidesWhenStopped = true
        activityIndicator.style = UIActivityIndicatorView.Style.gray
        activityIndicator.color = activityColor
        activityIndicator.startAnimating()
        self.isUserInteractionEnabled = false
        backgroundView.addSubview(activityIndicator)
        self.addSubview(backgroundView)
    }
    
    func activityStopAnimating() {
        if let background = viewWithTag(475647){
            background.removeFromSuperview()
        }
        self.isUserInteractionEnabled = true
    }
}

extension UIViewController {
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard(_:)))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
        
        if let nav = self.navigationController {
            nav.view.endEditing(true)
        }
    }
}
