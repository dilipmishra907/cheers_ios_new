
//  Utils.swift
//  SGDoc
//
//  Created by Spicejet LTD on 11/01/20.
//  Copyright © 2020 Spicejet LTD. All rights reserved.
//

import UIKit
import CoreLocation
import CoreTelephony
import SystemConfiguration
import SVProgressHUD
import NVActivityIndicatorView

enum cardMode : Int{
    case Active = 1
    case Escrow = 2
    case Dispute = 3
    case Sold = 4
    case Delete = 9
}
class Utils: NSObject {
    static let shared = Utils()
}

//MARK: - Alerts
extension Utils {
    
    class func Regular(sz : CGFloat) -> UIFont {
        return UIFont(name: "HelveticaNeue", size: sz)!
    }
    
    class func Medium(sz : CGFloat) -> UIFont {
        return UIFont(name: "HelveticaNeue-Medium", size: sz)!
    }
    
    class func Bold(sz : CGFloat) -> UIFont {
        return UIFont(name: "HelveticaNeue-Bold", size: sz)!
    }
    class func GothamProMedium(sz: CGFloat) -> UIFont {
        return UIFont(name: "GothamPro-Medium", size:sz)!
    }
    class func AvenirBlack(sz: CGFloat) -> UIFont {
        return UIFont(name: "Avenir-Black", size:sz)!
    }
    class func notReadyAlert() {
        let alert = UIAlertController(title: "In development!", message: "This component isn't ready", preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(action)
        alert.show()
    }

    
    class func alert(message: String, title: String? = "Cheers!") {
        DispatchQueue.main.async {
            
            let alert = UIAlertController(title: title,
                message: message,
                preferredStyle: UIAlertController.Style.alert)

            let cancelAction = UIAlertAction(title: "OK",
                                             style: .cancel, handler: nil)
            alert.addAction(cancelAction)
            UIApplication.shared.keyWindow?.rootViewController!.present(alert, animated: true,
                completion: nil)
            
//            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
//            let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
//            alert.addAction(action)
//            alert.show()
        }
    }
    
    class func alert(message: String, title: String, button1: String, button2: String? = nil, button3: String? = nil, action:@escaping (Int)->())
    {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            let action1 = UIAlertAction(title: button1, style: .default) { _ in
                action(0)
            }
            alert.addAction(action1)
            
            if button2 != nil {
                let action2 = UIAlertAction(title: button2, style: .default) { _ in
                    action(1)
                }
                alert.addAction(action2)
            }
            if button3 != nil {
                let action3 = UIAlertAction(title: button3, style: .default) { _ in
                    action(2)
                }
                alert.addAction(action3)
            }
            alert.show()
        }
    }
    class func getRandomColorNumber()-> Int {
        let random = Int(arc4random_uniform(255) + 1)
        return random
    }
    class func getRandomDeviceNumber()-> Int {
        let random = Int(arc4random_uniform(99999) + 1)
        return random
    }
    class func getRandomDeviceKeyNumber()-> Int {
        let random = Int(arc4random_uniform(999) + 1)
        return random
    }
    class func getAnimationRandomNumber()-> Int {
        let random = Int(arc4random_uniform(10) + 1)
        return random
    }
    class func getSaturationRadomNumber()-> Int {
        let random = Int(arc4random_uniform(100) + 1)
        return random
    }
    class func getAlphaRandomNumber()-> Int {
        let random = Int(arc4random_uniform(100) + 1)
        return random
    }
    class func getAnimationImageName(animationValue: Int)-> String {
        switch animationValue {
        case 0:
            return "Fade"
        case 1:
            return "comet"
        case 2:
            return "worm"
        case 3:
            return "constellation"
        case 4:
            return"jump arrow"
        case 5:
            return "Run"
        case 6:
            return "UFO"
        case 7:
            return "tri"
        case 8:
            return "tri1"
        case 9:
            return "tri2"
        default:
            return ""
        }
    }
}


//MARK: - Spinner
//extension Utils {
//    class func showSpinner() {
//        DispatchQueue.main.async {
//           SVProgressHUD.setDefaultMaskType(.gradient)
//           SVProgressHUD.setDefaultAnimationType(.native)
//           SVProgressHUD.show()
//        }
//    }
//    
//    class func hideSpinner() {
//        DispatchQueue.main.async {
//            SVProgressHUD.dismiss()
//        }
//    }
//}

//MARK: - Country Code
extension Utils {
    class func getCountryCallingCode() -> String? {
        let networkInfo = CTTelephonyNetworkInfo()
        let carrier = networkInfo.subscriberCellularProvider
        // Get carrier name
        let countryCode = carrier?.isoCountryCode
        return countryCode
    }
}

//MARK: - Sizing Functions
extension Utils {
    class func DeviceWidth() -> CGFloat {
        return UIScreen.main.bounds.size.width
    }
    
    class func DeviceHeight() -> CGFloat {
        return UIScreen.main.bounds.size.height
    }
}

//MARK: - UserDefault Functions
extension Utils {
    class func saveDataToUserDefault(_ data : Any, _ key : String) {
        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: data)
        UserDefaults.standard.set(encodedData, forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    class func getDataFromUserDefault(_ key : String) -> Any? {
        if let decoded  = UserDefaults.standard.object(forKey: key)  {
            let decodedTeams = NSKeyedUnarchiver.unarchiveObject(with: decoded as! Data)
            return decodedTeams
        }
        return nil
    }
    
    class func removeDataFromUserDefault(_ key : String) {
        UserDefaults.standard.removeObject(forKey: key)
        UserDefaults.standard.synchronize()
    }
}
//MARK: - Alert Extension
private var kAlertControllerWindow = "kAlertControllerWindow"
extension UIAlertController {
    
    var alertWindow: UIWindow? {
        get {
            return objc_getAssociatedObject(self, &kAlertControllerWindow) as? UIWindow
        }
        set {
            objc_setAssociatedObject(self, &kAlertControllerWindow, newValue, .OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    func show() {
        show(animated: true)
    }
    
    func show(animated: Bool) {
        alertWindow = UIWindow(frame: UIScreen.main.bounds)
        alertWindow?.rootViewController = UIViewController()
        alertWindow?.windowLevel = UIWindow.Level.alert + 1
        alertWindow?.makeKeyAndVisible()
        alertWindow?.rootViewController?.present(self, animated: animated, completion: nil)
    }
    
    open override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        alertWindow?.isHidden = true
        alertWindow = nil
    }
}

extension Utils {
    class func setNoDaFoundLabel(view: UIView?, message: String?) {
        let label = UILabel(frame: (view?.bounds)!)
        label.text = message
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 14.0)
        label.tag = 111
        view?.addSubview(label)
    }
    class func getRGBStringFromView(view: UIView) -> [Int] {
        let mainColor = view.subviews.first!.backgroundColor!
        let r1 = round(Double(Double(mainColor.components?.red ?? 1.0) * 255)).intValue
        let b1 = round(Double(Double(mainColor.components?.blue ?? 1.0) * 255)).intValue
        let g1 = round(Double(Double(mainColor.components?.green ?? 1.0) * 255)).intValue
        let secondColor = view.backgroundColor!
        let r2 = round(Double(Double(secondColor.components?.red ?? 1.0) * 255)).intValue
        let b2 = round(Double(Double(secondColor.components?.blue ?? 1.0) * 255)).intValue
        let g2 = round(Double(Double(secondColor.components?.green ?? 1.0) * 255)).intValue
        return [r1,g1,b1,r2,g2,b2]
    }
}

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
    var toHexString: String {
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        self.getRed(&r, green: &g, blue: &b, alpha: &a)
        
        return String(
            format: "%02X%02X%02X",
            Int(r * 0xff),
            Int(g * 0xff),
            Int(b * 0xff)
        )
    }
    var components: (red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat)? {
        var r: CGFloat = 0, g: CGFloat = 0, b: CGFloat = 0, a: CGFloat = 0
        return getRed(&r, green: &g, blue: &b, alpha: &a) ? (r,g,b,a) : nil
    }
}
extension Bundle {
    var versionNumber: String? {
        return infoDictionary?["CFBundleShortVersionString"] as? String
    }
    var buildNumber: String? {
        return infoDictionary?["CFBundleVersion"] as? String
    }
}
extension CALayer {
    
    func colorOfPoint(point:CGPoint) -> CGColor {
        
        var pixel: [CUnsignedChar] = [0, 0, 0, 0]
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
        let context = CGContext(data: &pixel, width: 1, height: 1, bitsPerComponent: 8, bytesPerRow: 4, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)
        context!.translateBy(x: -point.x, y: -point.y)
        self.render(in: context!)
        let red: CGFloat   = CGFloat(pixel[0]) / 255.0
        let green: CGFloat = CGFloat(pixel[1]) / 255.0
        let blue: CGFloat  = CGFloat(pixel[2]) / 255.0
        let alpha: CGFloat = CGFloat(pixel[3]) / 255.0
        
        let color = UIColor(red:red, green: green, blue:blue, alpha:alpha)
        
        return color.cgColor
    }
}
extension UIImageView {
    func calculateColor(point: CGPoint) -> UIColor {
        let center = self.center
        let radius = self.frame.width/2
        // Since the upper side of the screen for obtaining the coordinate difference
        // is set as the Y coordinate +, the sign of Y coordinate is replaced
        let x = point.x - center.x
        let y = -(point.y - center.y)
        
        // Find the radian angle
        var radian = atan2f(Float(y), Float(x))
        if radian < 0 {
            radian += Float(Double.pi*2)
        }
        
        let distance = CGFloat(sqrtf(Float(pow(Double(x), 2) + pow(Double(y), 2))))
        let saturation = (distance > radius) ? 1.0 : distance/radius
        let brightness = 1.0
        let alpha = 1.0
        let hue = CGFloat(radian/Float(Double.pi*2))
        return UIColor(hue: hue,
                       saturation: saturation,
                       brightness: CGFloat(brightness),
                       alpha: CGFloat(alpha))
    }
    
}
extension UIView {
    func roundCornerRadius() {
        self.layer.cornerRadius = self.frame.width / 2
        self.clipsToBounds = true
    }
}
extension Float {
    var intValue: Int {
        get {
            return Int(roundf(self))
        }
    }
}
extension CGFloat {
    var intValue: Int {
        get {
            return Int(roundf(Float(self)))
        }
    }
}
extension Int {
    var floatValue: CGFloat {
        get {
            return CGFloat(self)
        }
    }
}
extension Double {
    var intValue: Int {
        get {
            return Int(roundf(Float(self)))
        }
    }
}

//MARK: - Spinner
extension Utils {
    class func showSpinner() {
        DispatchQueue.main.async {
//           SVProgressHUD.setDefaultMaskType(.gradient)
//            SVProgressHUD.setDefaultAnimationType(.native)
//            SVProgressHUD.show()
//
           
            let activityData = ActivityData()
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)

            
        }
    }
    
    class func hideSpinner() {
        DispatchQueue.main.async {
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()

            
          //  SVProgressHUD.dismiss()
        }
    }
}

extension String {
    var intValue: Int {
        get {
            return Int(roundf(Float(self)!))
        }
    }
    func convertToDictionary() -> Any? {
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: [])
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    var trimText: String {
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
}

public class Reachability {
    class func isConnectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        if flags.isEmpty {
            return false
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }
}
